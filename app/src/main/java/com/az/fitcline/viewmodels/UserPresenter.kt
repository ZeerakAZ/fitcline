package com.az.fitcline.viewmodels

import android.app.Activity
import com.az.fitcline.model.dataaccess.entities.UserModel
import com.az.fitcline.model.dataaccess.network.APIsEndPoints
import com.az.fitcline.model.dataaccess.network.ApiClient
import com.az.fitcline.model.dataaccess.network.CallApi
import com.az.fitcline.model.essentials.SessionClass
import com.google.gson.Gson
import javax.inject.Inject

/**
 * Created by zeerak on 6/2/2018 bt ${File}
 */
class UserPresenter(activity: Activity, contract: BasePresenter.IPresenterContract?) : BasePresenter(activity, contract) {

    fun loginUser(email: String, password: String) {
        val login = ApiClient.getApiClient().login(token!!, email, password)
        apiCaller?.callService(login, APIsEndPoints.LOGIN)
    }

    fun signUpUser(user:UserModel) {
        user.token = token
        val signUp = ApiClient.getApiClient().signUp(user)
        apiCaller?.callService(signUp, APIsEndPoints.SIGNUP)
    }





    override fun onSuccess(body: String, endPoint: String) {
        super.onSuccess(body, endPoint)
        when (endPoint) {
            APIsEndPoints.SIGNUP,
            APIsEndPoints.LOGIN -> {
                val user = Gson().fromJson<UserModel>(body,UserModel::class.java)
                user.token = token
                SessionClass.getInstance().user = user


                if (contract is IUserListener) {
                    (contract as IUserListener).userFetched(user)
                }
            }
        }
    }

    public interface IUserListener : IPresenterContract {
        fun userFetched(user: UserModel)
    }
}

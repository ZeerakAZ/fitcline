package com.az.fitcline.viewmodels

import android.app.Activity
import com.az.fitcline.model.dataaccess.network.APIsEndPoints
import com.az.fitcline.model.dataaccess.network.ApiClient
import com.az.fitcline.model.essentials.SessionClass
import com.az.fitcline.model.utilities.AppUtitlity
import org.json.JSONException
import org.json.JSONObject

/**
 * Created by zeerak on 6/2/2018 bt ${File}
 */
class AppStartPresenter(activity: Activity, contract: IPresenterContract?) : BasePresenter(activity, contract) {

    private val platform = "android"




    private fun fetchToken(fcmToken:String?=null){
        val fetchToken = ApiClient.getApiClient().
                createSession(platform,AppUtitlity.getCultureCode(),
                        deviceToken = fcmToken)
        apiCaller?.loadingMessage = null
        apiCaller?.callService(fetchToken,APIsEndPoints.CREATE_SESSION)
    }

    fun checkForToken():Boolean{
        return if (token==null) {
            fetchToken()
            false
        }else{
            true
        }
    }

    fun isUserLoggedIn() : Boolean{
        return  SessionClass.getInstance().user!=null
    }

    override fun onSuccess(body: String, endPoint: String) {
        super.onSuccess(body, endPoint)
        when(endPoint){
            APIsEndPoints.CREATE_SESSION->{
                try {
                    val jObj = JSONObject(body)
                    val token = jObj.getString("token")
                    SessionClass.getInstance().token = token

                    if(contract is IAppStartListener)
                        (contract as IAppStartListener).tokenFetched()

                } catch (e: JSONException) {
                    e.printStackTrace()
                }

            }
        }
    }

    public interface IAppStartListener:IPresenterContract{
        fun tokenFetched()
    }

}
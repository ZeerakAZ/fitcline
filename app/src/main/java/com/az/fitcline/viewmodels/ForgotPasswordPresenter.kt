package com.az.fitcline.viewmodels

import android.app.Activity
import com.az.fitcline.model.dataaccess.network.APIsEndPoints
import com.az.fitcline.model.dataaccess.network.ApiClient
import org.json.JSONObject

/**
 * Created by zeerak on 6/23/2018 bt ${File}
 */
class ForgotPasswordPresenter(activity: Activity, contract: IPresenterContract?) : BasePresenter(activity, contract) {
    private lateinit var mUserId: String
    private lateinit var mVerifyCode: String

    fun requestPassword(email: String) {
        ApiClient.getApiClient().requestForgotPassword(token!!, email).run {
            apiCaller?.callService(this, APIsEndPoints.REQUEST_PASSWORD_RECOVERY)
        }
    }

    fun verifyCode(verifyCode:String){
        mVerifyCode = verifyCode
        ApiClient.getApiClient().verifyPasswordRecovery(token!!,verifyCode,mUserId).run {
            apiCaller?.callService(this,APIsEndPoints.VERIFY_PASSWORD_RECOVERY)
        }
    }

    fun updatePassword(newPassword: String){
        ApiClient.getApiClient().chagePassword(token!!,mVerifyCode,mUserId,newPassword).run {
            apiCaller?.callService(this,APIsEndPoints.CHANGE_PASSWORD)
        }
    }


    override fun onSuccess(body: String, endPoint: String) {
        super.onSuccess(body, endPoint)
        (contract as? IPasswordUpdateListener)?.apply {
            when (endPoint) {
                APIsEndPoints.REQUEST_PASSWORD_RECOVERY -> {
                    JSONObject(body).apply {
                        getString("userId").run {
                            mUserId = this
                            onRequestPassword(this)
                        }
                    }
                }
                APIsEndPoints.VERIFY_PASSWORD_RECOVERY -> {
                    onVerifyCode(true)
                }
                APIsEndPoints.CHANGE_PASSWORD -> {
                    onPasswordUpdated(true)
                }
            }
        }
    }

    interface IPasswordUpdateListener : IPresenterContract {
        fun onRequestPassword(userId: String)
        fun onVerifyCode(isSuccess: Boolean)
        fun onPasswordUpdated(isSuccess: Boolean)
    }
}

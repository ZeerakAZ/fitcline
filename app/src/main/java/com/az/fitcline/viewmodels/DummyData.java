package com.az.fitcline.viewmodels;

import com.az.fitcline.model.dataaccess.adapters.GroupMemberAdapter;
import com.az.fitcline.model.dataaccess.entities.CommentModel;
import com.az.fitcline.model.dataaccess.entities.GroupMember;
import com.az.fitcline.model.dataaccess.entities.GroupModel;
import com.az.fitcline.model.dataaccess.entities.MessageModel;
import com.az.fitcline.model.dataaccess.entities.NotificationModel;
import com.az.fitcline.model.dataaccess.entities.PostModel;
import com.az.fitcline.model.dataaccess.entities.UserModel;
import com.az.fitcline.model.dataaccess.entities.WorkOutPostModel;
import com.az.fitcline.model.dataaccess.entities.WorkoutMemberModel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Zeera on 12/12/2017 bt ${File}
 */

public class DummyData {
    private static ArrayList<PostModel> postModels;
    private static ArrayList<WorkOutPostModel> workOutPostModels;
    private static Set<UserModel> mUsers;
    private static ArrayList<MessageModel> mMessages;
    private static ArrayList<GroupModel> mGroups;
    private static ArrayList<GroupMember> mRandomGroupMembers;
    private static ArrayList<WorkoutMemberModel> mWorkoutsMember;
    private static ArrayList<NotificationModel> mNotifications;

    public static ArrayList<PostModel> getPostsRandom() {
        if (postModels == null) {
            postModels = new ArrayList<>();
            mUsers = new HashSet<>();

            int randomInt = (int) ((Math.random() * 100) + 5);
            for (int i = 0; i < randomInt; i++) {
                PostModel randomPosts = PostModel.getRandomPosts();
                postModels.add(randomPosts);
                mUsers.add(randomPosts.getUserModel());
            }

        }
        return postModels;
    }

    public static ArrayList<UserModel> getmUsers() {
        if (mUsers == null)
            getPostsRandom();
        return new ArrayList<>(mUsers);
    }

    public static ArrayList<WorkOutPostModel> getRandomWorkOuts() {
        if (workOutPostModels == null) {
            workOutPostModels = new ArrayList<>();


            int randomInt = (int) ((Math.random() * 100) + 5);
            UserModel userModel = UserModel.getRandomUser();
            for (int i = 0; i < randomInt; i++) {
                WorkOutPostModel randomPosts = WorkOutPostModel.getWorkOutModel(userModel);
                workOutPostModels.add(randomPosts);
            }

        }
        return workOutPostModels;
    }


    public static ArrayList<MessageModel> getDummyMessages() {

        if (mMessages == null) {
            mMessages = new ArrayList<>();

            int randomInt = (int) ((Math.random() * 100) + 5);
            for (int i = 0; i < randomInt; i++) {
                mMessages.add(MessageModel.getRandomMessage(UserModel.getRandomUser()));
            }

        }
        return mMessages;
    }

    public static ArrayList<GroupModel> getDummyGroups() {
        if (mGroups == null) {
            mGroups = new ArrayList<>();

            int randomInt = (int) ((Math.random() * 100) + 5);
            for (int i = 0; i < randomInt; i++) {
                mGroups.add(GroupModel.getRandomGroup());
            }

        }
        return mGroups;
    }

    public static ArrayList<GroupMember> getDummyGroupMember(){
        if (mRandomGroupMembers == null) {
            mRandomGroupMembers = new ArrayList<>();

            int randomInt = (int) ((Math.random() * 100) + 5);
            for (int i = 0; i < randomInt; i++) {
                mRandomGroupMembers.add(GroupMember.getRandomGroupMember());
            }

        }
        return mRandomGroupMembers;
    }

    public static ArrayList<WorkoutMemberModel> getmWorkoutsMember() {
        if (mWorkoutsMember == null) {
            mWorkoutsMember = new ArrayList<>();

            int randomInt = (int) ((Math.random() * 100) + 5);
            for (int i = 0; i < randomInt; i++) {
                mWorkoutsMember.add(WorkoutMemberModel.getWorkoutModel());
            }

        }
        return mWorkoutsMember;
    }

    public static ArrayList<NotificationModel> getNotifications(){
        if (mNotifications == null) {
            mNotifications = new ArrayList<>();

            int randomInt = (int) ((Math.random() * 100) + 5);
            for (int i = 0; i < randomInt; i++) {
                mNotifications.add(NotificationModel.getRandomNotification());
            }

        }
        return mNotifications;
    }
}

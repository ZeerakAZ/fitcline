package com.az.fitcline.viewmodels

import android.app.Activity
import android.content.Context

import com.az.fitcline.R
import com.az.fitcline.model.dataaccess.network.CallApi
import com.az.fitcline.model.dataaccess.network.IResponse
import com.az.fitcline.model.essentials.SessionClass
import com.az.fitcline.views.activities.BaseActivity


/**
 * Created by Zeera on 3/11/2018 bt ${File}
 */

open class BasePresenter : IResponse {

    var activity: Activity? = null

    var context: Context? = null
        private set

    internal var apiCaller: CallApi? = null
        private set

    var contract: IPresenterContract? = null
        private set

    val token: String?
        get() {
            return if (SessionClass.getInstance().user == null) {
                SessionClass.getInstance().token
            } else {
                SessionClass.getInstance().user?.token
            }
        }


    internal constructor(activity: Activity, contract: IPresenterContract?) {
        if (activity !is BaseActivity)
            throw IllegalArgumentException("invalid activity")
        this.activity = activity
        this.contract = contract
        this.context = activity

        apiCaller = CallApi(activity, this)
        apiCaller?.loadingMessage = activity.getString(R.string.message_wait)


    }

    internal constructor(context: Context, contract: IPresenterContract?) {
        apiCaller = CallApi(context, this)
        this.contract = contract
        this.context = context
    }

    internal fun getActivity(): BaseActivity? {
        return activity as BaseActivity
    }


    interface IPresenterContract

    override fun onSuccess(body: String, endPoint: String) {}

    override fun onError(error: String, endPoint: String) {}

    override fun onFailed(e: Throwable, endPoint: String) {}
}

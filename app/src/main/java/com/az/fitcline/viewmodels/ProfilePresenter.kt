package com.az.fitcline.viewmodels

import android.app.Activity
import com.az.fitcline.model.dataaccess.network.APIsEndPoints
import com.az.fitcline.model.dataaccess.network.ApiClient
import com.az.fitcline.model.essentials.service.FileUploadService

/**
 * Created by zeerak on 6/24/2018 bt ${File}
 */
class ProfilePresenter(activity: Activity,contract: BasePresenter.IPresenterContract):BasePresenter(activity,contract) {


    fun uploadImage(path:String){
        FileUploadService.startUploading(context!!,path,null)
    }

    fun completeProfile(pictureId:String,gender:Int,birthDate:Long,units:Int){
        ApiClient.getApiClient().completeProfile(pictureId,gender,birthDate,units).run {
            apiCaller?.callService(this,APIsEndPoints.COMPLETE_PROFILE)
        }
    }

    override fun onSuccess(body: String, endPoint: String) {
        super.onSuccess(body, endPoint)
        (contract as? IProfileCompleteListener)?.run {
            when(endPoint){
                APIsEndPoints.COMPLETE_PROFILE->{
                    profileUpdated(true)
                }
            }
        }
    }

    public interface IProfileCompleteListener:IPresenterContract{
        fun profileUpdated(isSuccess:Boolean)
    }
}

package com.az.fitcline.views.activities.SignUpModule;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ViewFlipper;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.validations.EmptyValidation;
import com.az.fitcline.model.dataaccess.validations.TextCompareValidation;
import com.az.fitcline.model.dataaccess.validations.Validation;
import com.az.fitcline.model.dataaccess.validations.Validator;
import com.az.fitcline.model.utilities.ActivityNavigationUtility;
import com.az.fitcline.model.utilities.ToastUtility;
import com.az.fitcline.viewmodels.ForgotPasswordPresenter;
import com.az.fitcline.views.activities.BaseActivity;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgotPasswordActivity extends BaseActivity implements ForgotPasswordPresenter.IPasswordUpdateListener {

    @BindView(R.id.vs_forgot_password)
    ViewFlipper mVsForgotPassword;
    @BindView(R.id.et_code)
    EditText mEtCode;
    @BindView(R.id.et_email)
    EditText mEtEmail;
    @BindView(R.id.et_password)
    EditText mEtPassword;
    @BindView(R.id.et_password_again)
    EditText mEtPasswordAgain;
    @BindView(R.id.btn_next)
    Button mBtnNext;

    private ForgotPasswordPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_passwor);
        ButterKnife.bind(this);
        mVsForgotPassword.setInAnimation(this,R.anim.slide_in_from_right);
        mVsForgotPassword.setOutAnimation(this,R.anim.slide_out_to_left);

        mPresenter = new ForgotPasswordPresenter(this,this);
    }

    @OnClick(R.id.btn_next)
    public void onClick() {
        switch (mVsForgotPassword.getCurrentView().getId()) {
            case R.id.ll_step_1:
                if (new Validator(this).validates(new EmptyValidation(getString(R.string.error_email_or_username),
                        mEtEmail, getString(R.string.ph_error)))) {
                    if (mPresenter != null) {
                        mPresenter.requestPassword(mEtEmail.getText().toString());
                    }
                }
                break;
            case R.id.ll_step_2:
                if (new Validator(this).validates(new EmptyValidation(getString(R.string.error_code),
                        mEtCode, getString(R.string.ph_error)))) {
                    if(mPresenter!=null){
                        mPresenter.verifyCode(mEtCode.getText().toString());
                    }
                }
                break;
            case R.id.ll_step_3:
                if (new Validator(this).validates(getStep3validations())) {
                    if (mPresenter != null) {
                        mPresenter.updatePassword(mEtPasswordAgain.getText().toString());
                    }
                }
                break;
        }

    }

    private ArrayList<Validation> getStep3validations(){
        ArrayList<Validation> validation = new ArrayList<>();
        validation.add(new EmptyValidation(getString(R.string.error_password), mEtPassword, getString(R.string.ph_error)));
        validation.add(new EmptyValidation(getString(R.string.error_password_again), mEtPasswordAgain, getString(R.string.ph_error)));
        validation.add(new TextCompareValidation(getString(R.string.error_password_compare), mEtPasswordAgain,mEtPassword.getText().toString()
                , getString(R.string.ph_error)).setShouldBeEqual(true));
        return validation;
    }

    @OnClick(R.id.tv_back)
    public void onBack() {
        onBackPressed();
    }


    @Override
    public void onBackPressed() {
        if (mVsForgotPassword.getCurrentView().getId() == R.id.ll_step_2
                ||mVsForgotPassword.getCurrentView().getId() == R.id.ll_step_3) {
            mBtnNext.setText(R.string.ph_next);
            mVsForgotPassword.showPrevious();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onRequestPassword(@NotNull String userId) {
        mVsForgotPassword.showNext();
    }

    @Override
    public void onVerifyCode(boolean isSuccess) {
        if(isSuccess){
            mBtnNext.setText(R.string.ph_done);
            mVsForgotPassword.showNext();
            ToastUtility.showToastForLongTime(this,getString(R.string.messsage_code_send));
        }
    }

    @Override
    public void onPasswordUpdated(boolean isSuccess) {
        if(isSuccess){
            ActivityNavigationUtility.navigateWith(this)
                    .setClearStack().navigateTo(LoginActivity.class);
            ToastUtility.showToastForLongTime(this,getString(R.string.message_password_updated));
        }

    }
}

package com.az.fitcline.views.fragments.leaderboardmodule;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.adapters.WorkoutListAdapter;
import com.az.fitcline.model.dataaccess.adapters.WorkoutMemberAdapter;
import com.az.fitcline.viewmodels.DummyData;
import com.az.fitcline.views.fragments.BaseFragment;

import butterknife.BindView;

public class WorkoutLeaderFragment extends BaseFragment {
    private static final String TAG = "workoutleader";
    @BindView(R.id.rv_workouts)
    RecyclerView mRvWorkout;

    public WorkoutLeaderFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_workout_leader, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAdapter();
    }

    private void setAdapter() {
        mRvWorkout.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvWorkout.setHasFixedSize(true);
        mRvWorkout.setAdapter(new WorkoutMemberAdapter(getContext(),DummyData.getmWorkoutsMember()));
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return null;
    }
}

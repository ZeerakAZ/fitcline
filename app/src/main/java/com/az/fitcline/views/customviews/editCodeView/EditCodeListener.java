package com.az.fitcline.views.customviews.editCodeView;

public interface EditCodeListener {
    void onCodeReady(String code);
}


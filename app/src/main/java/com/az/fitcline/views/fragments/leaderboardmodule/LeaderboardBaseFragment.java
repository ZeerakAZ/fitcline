package com.az.fitcline.views.fragments.leaderboardmodule;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.adapters.TabAdapter;
import com.az.fitcline.model.dataaccess.entities.TabModel;
import com.az.fitcline.model.dataaccess.enums.BottomMenuItems;
import com.az.fitcline.model.dataaccess.interfaces.IFragmentBottomTag;
import com.az.fitcline.views.customviews.CustomTabLayout;
import com.az.fitcline.views.fragments.BaseFragment;
import com.az.fitcline.views.fragments.feedmodule.WorkoutListFragment;

import javax.annotation.Nullable;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class LeaderboardBaseFragment extends BaseFragment implements IFragmentBottomTag {
    private static final String TAG = "leaderBoard";
    @BindView(R.id.vp_workout)
    ViewPager mVpWorkout;
    @BindView(R.id.tb_workout)
    CustomTabLayout mTbWorkout;

    public LeaderboardBaseFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_leaderboard, container, false);
    }

    @Override
    public void onViewCreated(View view, @android.support.annotation.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setWorkoutTabs();
    }

    private void setWorkoutTabs(){
        TabAdapter adapter = new TabAdapter(getChildFragmentManager());

        adapter.addTab(new TabModel(new WorkoutLeaderFragment(),"Bench"),
        new TabModel(new WorkoutLeaderFragment(),"Squat"),
        new TabModel(new WorkoutLeaderFragment(),"Deadlift"));
        mVpWorkout.setAdapter(adapter);
        mTbWorkout.setupWithViewPager(mVpWorkout);
    }

    @Nullable
    @Override
    public BottomMenuItems getMenuItem() {
        return BottomMenuItems.LEADER_BOARD;
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return getString(R.string.title_leaderboard);
    }
}

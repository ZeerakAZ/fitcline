package com.az.fitcline.views.fragments.memodule;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.adapters.MessageListAdapter;
import com.az.fitcline.model.dataaccess.adapters.NotificationAdapter;
import com.az.fitcline.viewmodels.DummyData;
import com.az.fitcline.views.fragments.BaseFragment;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends BaseFragment {
    public static final String TAG = "notifications";
    @BindView(R.id.rv_notifications)
    RecyclerView mRvNotifications;

    public NotificationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notification, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setNotifications();
    }

    private void setNotifications() {
        mRvNotifications.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvNotifications.setAdapter(new NotificationAdapter(getContext(), DummyData.getNotifications()));
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return getString(R.string.title_notification);
    }
}

package com.az.fitcline.views.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.enums.BottomMenuItems;
import com.az.fitcline.model.dataaccess.interfaces.IFragmentBottomTag;

import javax.annotation.Nullable;

/**
 * A simple {@link Fragment} subclass.
 */
public class WorkoutFragment extends BaseFragment implements IFragmentBottomTag {


    public WorkoutFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_workout, container, false);
    }

    @Nullable
    @Override
    public BottomMenuItems getMenuItem() {
        return BottomMenuItems.WORKOUT;
    }

    @Override
    public String getTagFragment() {
        return null;
    }

    @Override
    public String getTitle() {
        return getString(R.string.title_workout);
    }
}

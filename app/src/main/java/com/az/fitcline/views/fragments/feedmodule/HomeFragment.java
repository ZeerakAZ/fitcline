package com.az.fitcline.views.fragments.feedmodule;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.adapters.PostAdapter;
import com.az.fitcline.model.dataaccess.entities.PostModel;
import com.az.fitcline.model.dataaccess.enums.BottomMenuItems;
import com.az.fitcline.model.dataaccess.enums.FragmentAnimationType;
import com.az.fitcline.model.dataaccess.interfaces.IFragmentBottomTag;
import com.az.fitcline.model.dataaccess.interfaces.IRightActionOption;
import com.az.fitcline.model.utilities.FragmentUtility;
import com.az.fitcline.viewmodels.DummyData;
import com.az.fitcline.views.fragments.BaseFragment;

import javax.annotation.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends BaseFragment implements IFragmentBottomTag ,IRightActionOption{
    public static final String TAG = "home";
    private Unbinder unbinder;
    @BindView(R.id.rv_posts)
    RecyclerView mRvPosts;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, @android.support.annotation.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        setAdapter();
    }

    public void setAdapter() {
        mRvPosts.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvPosts.setAdapter(new PostAdapter(getContext(), DummyData.getPostsRandom(), postModel -> {
            FragmentUtility.withManager(getActivity().getSupportFragmentManager())
                    .addToBackStack(getTagFragment())
                    .withAnimationType(FragmentAnimationType.SLIDE_FROM_RIGHT)
                    .replaceToFragment(PostFragment.newInstance(postModel));
        }));

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }


    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return getString(R.string.title_home);
    }

    @Nullable
    @Override
    public BottomMenuItems getMenuItem() {
        return BottomMenuItems.FEED;
    }

    @OnClick(R.id.ll_add_post)
    public void toAddPost() {
        FragmentUtility.withManager(getFragmentManager())
                .withAnimationType(FragmentAnimationType.GROW_FROM_BOTTOM)
                .addToBackStack(getTagFragment())
                .replaceToFragment(new PostingFragment());
    }

    @Override
    public int getRightActionImageId() {
        return R.drawable.people;
    }

    @Override
    public void performRightAction() {
        FragmentUtility.withManager(getActivity().getSupportFragmentManager())
                    .addToBackStack(getTagFragment())
                    .withAnimationType(FragmentAnimationType.GROW_FROM_BOTTOM)
                    .replaceToFragment(UserSearchFragment.newInstance(getString(R.string.title_search_people)));
    }
}

package com.az.fitcline.views.dialogs;

import android.content.DialogInterface;

/**
 * Created by Zeera on 6/28/2017 bt ${File}
 */

public interface IDialogDouble {
    void onRightClick(DialogInterface dialogInterface);
    void onLeftClick(DialogInterface dialogInterface);
}

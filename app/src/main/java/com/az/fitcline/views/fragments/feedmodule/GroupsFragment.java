package com.az.fitcline.views.fragments.feedmodule;


import android.nfc.Tag;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.adapters.GroupAdapter;
import com.az.fitcline.model.dataaccess.entities.GroupModel;
import com.az.fitcline.model.dataaccess.enums.FragmentAnimationType;
import com.az.fitcline.model.utilities.FragmentUtility;
import com.az.fitcline.viewmodels.DummyData;
import com.az.fitcline.views.fragments.BaseFragment;
import com.az.fitcline.views.fragments.workoutmodule.CreateWorkoutFragment;
import com.az.fitcline.views.fragments.workoutmodule.CreateWorkoutPostFragment;

import java.lang.annotation.Target;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class GroupsFragment extends BaseFragment {
    private static final String TAG = "groupScreen";

    @BindView(R.id.rv_groups)
    RecyclerView mRvGroups;
    private Unbinder unbinder;


    public GroupsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_groups, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        setGroups();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    private void setGroups(){
        mRvGroups.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvGroups.setHasFixedSize(true);
        mRvGroups.setAdapter(new GroupAdapter(getContext(), DummyData.getDummyGroups(), new GroupAdapter.IListViewOptions() {
            @Override
            public void onSettingClicked(GroupModel groupModel) {
                FragmentUtility.withManager(getFragmentManager())
                        .withAnimationType(FragmentAnimationType.SLIDE_FROM_LEFT)
                        .addToBackStack(getTagFragment())
                        .replaceToFragment(GroupSettingFragment.newInstance(groupModel));
            }

            @Override
            public void onDeleteClicked(GroupModel groupModel) {

            }
        }));
    }

    @OnClick(R.id.ll_home)
    public void goBackToHome(){
        getActivity().onBackPressed();
    }

    @OnClick(R.id.ll_create_group)
    public void toCreateGroup(){
        FragmentUtility.withManager(getFragmentManager())
                .withAnimationType(FragmentAnimationType.GROW_FROM_BOTTOM)
                .addToBackStack(getTagFragment())
                .replaceToFragment(new CreateGroupFragment());
    }



    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return "";
    }
}

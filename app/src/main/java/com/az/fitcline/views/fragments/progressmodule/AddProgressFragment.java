package com.az.fitcline.views.fragments.progressmodule;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.adapters.ExerciseAdapter;
import com.az.fitcline.model.dataaccess.adapters.WorkoutTypeAdapter;
import com.az.fitcline.model.dataaccess.enums.ExerciseType;
import com.az.fitcline.model.dataaccess.enums.ProgressType;
import com.az.fitcline.views.fragments.BaseFragment;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddProgressFragment extends BaseFragment implements WorkoutTypeAdapter.ITypeListener, ExerciseAdapter.ITypeListener {
    public static final String TAG = "add_progress";
    @ProgressType
    public int mType = ProgressType.TYPE_WEIGHT_PICTURE;

    @BindView(R.id.rv_workout_type)
    RecyclerView mRvWorkoutType;
    @BindView(R.id.rv_exercise_type)
    RecyclerView mRvExercise;
    @BindView(R.id.ll_pictures)
    LinearLayout mLlPictures;
    @BindView(R.id.ll_count_in)
    LinearLayout mLlCountIn;
    @BindView(R.id.tv_exercise)
    TextView mTvExercise;



    public AddProgressFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_progress_strength, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTypeAdapter();
    }

    private void setTypeAdapter() {

        mRvWorkoutType.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvWorkoutType.setAdapter(new WorkoutTypeAdapter(getContext(),this));

        mRvExercise.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvExercise.setAdapter(new ExerciseAdapter(getContext(),this));

    }


    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public void typeUpdated(int type) {
        switch (type) {
            case ProgressType.TYPE_WEIGHT_PICTURE:
                mLlCountIn.setVisibility(View.GONE);
                mLlPictures.setVisibility(View.VISIBLE);
                mTvExercise.setVisibility(View.GONE);
                mRvExercise.setVisibility(View.GONE);
                break;
            case ProgressType.TYPE_STRENGTH:
                mLlCountIn.setVisibility(View.VISIBLE);
                mLlPictures.setVisibility(View.GONE);
                mTvExercise.setVisibility(View.VISIBLE);
                mRvExercise.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void typeUpdated(ExerciseType type) {
        switch (type){
            case BENCH:
                break;
            case SQUAT:
                break;
            case DEAD_LIFT:
                break;
        }
    }
}

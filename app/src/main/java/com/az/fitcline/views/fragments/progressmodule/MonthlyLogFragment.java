package com.az.fitcline.views.fragments.progressmodule;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.adapters.MonthlyLogAdapter;
import com.az.fitcline.model.dataaccess.entities.ProgressMonthlyLog;
import com.az.fitcline.model.dataaccess.enums.FragmentAnimationType;
import com.az.fitcline.model.utilities.FragmentUtility;
import com.az.fitcline.views.fragments.BaseFragment;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class MonthlyLogFragment extends BaseFragment {
    public static final String TAG = "overview";


    @BindView(R.id.rv_monthly_log)
    RecyclerView mMonthlyLog;

    public MonthlyLogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_monthly_log, container, false);
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setLogsAdapter();

    }

    private void setLogsAdapter() {
        mMonthlyLog.setLayoutManager(new LinearLayoutManager(getContext()));
        mMonthlyLog.setAdapter(new MonthlyLogAdapter((Activity) getContext(),
                ProgressMonthlyLog.getMonthlyLog(getContext(), false), log ->

                FragmentUtility.withManager(getFragmentManager())
                        .withAnimationType(FragmentAnimationType.SLIDE_FROM_RIGHT)
                        .addToBackStack(TAG)
                        .replaceToFragment(new EditLogFragment())));

    }


    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return null;
    }
}

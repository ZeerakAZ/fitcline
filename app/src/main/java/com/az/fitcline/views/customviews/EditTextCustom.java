package com.az.fitcline.views.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.validations.TextValidationWatcher;

/**
 * Created by Zeera on 12/31/2017 bt ${File}
 */

public class EditTextCustom extends LinearLayout {
    private int mDrawableLeft,mButtonText,mInputType;

    public EditTextCustom(Context context) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.edit_text_custtom,this);
    }

    public EditTextCustom(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public EditTextCustom(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }


    private void init(AttributeSet attrs) {
        setOrientation(VERTICAL);
        LayoutInflater.from(getContext()).inflate(R.layout.edit_text_custtom,this);
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs,R.styleable.EditTextCustom);
        mButtonText = typedArray.getResourceId(R.styleable.EditTextCustom_edit_text,R.string.ph_save);
        mDrawableLeft = typedArray.getResourceId(R.styleable.EditTextCustom_edittext_left_drawable,R.drawable.edit_icon);
        mInputType = typedArray.getInteger(R.styleable.EditTextCustom_inputType, InputType.TYPE_NULL);
        typedArray.recycle();


    }
    EditText et;

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        et = findViewById(R.id.et);
        et.setHint(mButtonText);
        et.setInputType(mInputType);
        ImageView imageView = findViewById(R.id.iv_left);
        imageView.setImageResource(mDrawableLeft);
        View divider = findViewById(R.id.divider);
        et.setOnFocusChangeListener((v, hasFocus) -> divider.setBackgroundColor(ContextCompat.getColor(getContext(),
                hasFocus?R.color.colorAccent:R.color.ashgrey)));
    }

    public void setHint(@StringRes int stringId){
        et.setHint(stringId);
    }


    public void addTextChangeListener(IEditTextChangeListener listener){
        et.addTextChangedListener(new TextValidationWatcher(et) {
            @Override
            public void validate(TextView textView, String text) {
                if (listener != null) {
                    listener.textUpdated(text);
                }
            }
        });

    }

    public interface IEditTextChangeListener{
        void textUpdated(String updateText);
    }

}

package com.az.fitcline.views.fragments.feedmodule;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.adapters.CommentsAdapter;
import com.az.fitcline.model.dataaccess.entities.BottomSheetOptionModel;
import com.az.fitcline.model.dataaccess.entities.PostModel;
import com.az.fitcline.model.dataaccess.enums.FragmentAnimationType;
import com.az.fitcline.model.dataaccess.interfaces.BottomOptionActions;
import com.az.fitcline.model.dataaccess.interfaces.IRightActionOption;
import com.az.fitcline.model.utilities.FragmentUtility;
import com.az.fitcline.views.dialogs.GenericBottomSheetDialog;
import com.az.fitcline.views.fragments.BaseFragment;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostFragment extends BaseFragment implements
        GenericBottomSheetDialog.IBottomOptionSelectionListener, IRightActionOption {
    public static final String TAG = "postfragment";
    private static final String ARG_POST = "arg_post";

    private Unbinder bind;

    @BindView(R.id.rv_comments)
    RecyclerView mRvComments;
    @BindView(R.id.tv_username)
    TextView mTvUserName;
    @BindView(R.id.tv_post_time)
    TextView mTvPostTime;
    @BindView(R.id.tv_like_count)
    TextView mTvLikesCount;
    @BindView(R.id.tv_post)
    TextView mTvPost;

    private PostModel mPost;


    public static PostFragment newInstance(PostModel postModel) {

        Bundle args = new Bundle();
        args.putSerializable(ARG_POST, postModel);
        PostFragment fragment = new PostFragment();
        fragment.setArguments(args);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey(ARG_POST))
            mPost = (PostModel) getArguments().getSerializable(ARG_POST);
        else
            throw new IllegalArgumentException("invalid params ..GTH");
    }

    public PostFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_post, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bind = ButterKnife.bind(this, view);
        setData();
    }

    private void setData() {
        mTvUserName.setText(mPost.getUserModel().getUserName());
        mTvPostTime.setText("4 mins");
        mTvPost.setText(mPost.getPost());
        mTvPostTime.setText(mPost.getPostTime());
        mTvLikesCount.setText(String.format(Locale.ENGLISH, "%d likes", mPost.getLikesCount()));
        mRvComments.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvComments.setAdapter(new CommentsAdapter(getContext(), mPost.getComments(),
                CommentsAdapter.TYPE_COMMENT_DETAILED));
    }

    @OnClick(R.id.tv_like_count)
    public void toLikeListFragment(){
        FragmentUtility.withManager(getActivity().getSupportFragmentManager())
                .withAnimationType(FragmentAnimationType.GROW_FROM_BOTTOM)
                .addToBackStack(getTagFragment())
                .replaceToFragment(LikesListFragment.newInstance());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        bind.unbind();
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return "";
    }

    @Override
    public void performActionPlease(int actionId) {
        switch (actionId) {
            case BottomOptionActions.DELETE_COMMENT:
                break;
            case BottomOptionActions.EDIT_COMMENT:
                break;

            case BottomOptionActions.REPORT_SPAM:
                break;
        }
    }

    public void showBottomOptions() {
        ArrayList<BottomSheetOptionModel> bottomSheetOptionModels = new ArrayList<>();
        bottomSheetOptionModels.add(BottomOptionActions.ACTION_DELETE_COMMENT);
        bottomSheetOptionModels.add(BottomOptionActions.ACTION_EDIT_COMMENT);
        new GenericBottomSheetDialog(getContext(), bottomSheetOptionModels, this).show();
    }

    @Override
    public int getRightActionImageId() {
        return R.drawable.overflow_white;
    }

    @Override
    public void performRightAction() {
        showBottomOptions();
    }

    @OnClick({R.id.rl_user_details,R.id.iv_profile_pic})
    public void toProfileScreen(){
        FragmentUtility.withManager(getActivity().getSupportFragmentManager())
                .withAnimationType(FragmentAnimationType.GROW_FROM_BOTTOM)
                .addToBackStack(getTagFragment())
                .replaceToFragment(new UserProfileFragment());
    }
}

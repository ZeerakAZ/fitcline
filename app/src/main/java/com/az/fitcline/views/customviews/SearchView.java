package com.az.fitcline.views.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.az.fitcline.R;

/**
 * Created by Zeera on 12/30/2017 bt ${File}
 */

public class SearchView extends RelativeLayout {
    EditText mEtSearch;

    public SearchView(Context context) {
        super(context);
        initializeViews(context,null);
    }

    public SearchView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initializeViews(context,attrs);
    }

    public SearchView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initializeViews(context,attrs);

    }


    private void initializeViews(Context context, AttributeSet attrs) {
        LayoutInflater.from(context).inflate(R.layout.layout_search_view, this);
        mEtSearch = findViewById(R.id.et_search);
        if (attrs != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.SearchView);
            int hintText = typedArray.getResourceId(R.styleable.SearchView_hint_text, R.string.ph_search);
            mEtSearch.setHint(hintText);
            typedArray.recycle();

        }
    }


    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
    }

}

package com.az.fitcline.views.activities

import android.app.ActivityOptions
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.widget.ImageView

import com.az.fitcline.R
import com.az.fitcline.model.utilities.ActivityNavigationUtility
import com.az.fitcline.views.activities.SignUpModule.LoginActivity

import java.util.concurrent.TimeUnit

import butterknife.BindView
import butterknife.ButterKnife
import com.az.fitcline.viewmodels.AppStartPresenter
import kotlinx.android.synthetic.main.activity_login.*

class SplashActivity : BaseActivity(), AppStartPresenter.IAppStartListener {

    companion object {
        private val DELAY_MILLIS = TimeUnit.SECONDS.toMillis(2)
    }

    private var isToProceed: Boolean = false
    private var mAppStartPresenter: AppStartPresenter? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        ButterKnife.bind(this)

        Handler().postDelayed({ this.proceed() }, DELAY_MILLIS)

        mAppStartPresenter = AppStartPresenter(this, this)
        isToProceed = mAppStartPresenter?.checkForToken() ?: false
    }

    private fun proceed() {
        if (isToProceed) {
            var options: ActivityOptions? = null
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                options = ActivityOptions.makeSceneTransitionAnimation(this, iv_logo,
                        this@SplashActivity.getString(R.string.transition_logo))
            }

            ActivityNavigationUtility.navigateWith(this@SplashActivity).setClearStack().setActivityOption(options).navigateTo(
                    if (mAppStartPresenter?.isUserLoggedIn() == false) LoginActivity::class.java else MainActivity::class.java
            )

            overridePendingTransition(0, 0)
        } else {
            isToProceed = true
        }
    }


    override fun tokenFetched() {
        proceed()
    }
}

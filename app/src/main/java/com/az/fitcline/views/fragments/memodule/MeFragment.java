package com.az.fitcline.views.fragments.memodule;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.adapters.MessageListAdapter;
import com.az.fitcline.model.dataaccess.adapters.NotificationAdapter;
import com.az.fitcline.model.dataaccess.adapters.PostAdapter;
import com.az.fitcline.model.dataaccess.entities.UserModel;
import com.az.fitcline.model.dataaccess.enums.BottomMenuItems;
import com.az.fitcline.model.dataaccess.enums.FragmentAnimationType;
import com.az.fitcline.model.dataaccess.interfaces.IFragmentBottomTag;
import com.az.fitcline.model.dataaccess.interfaces.IRightActionOption;
import com.az.fitcline.model.dataaccess.interfaces.IleftActionOption;
import com.az.fitcline.model.utilities.FragmentUtility;
import com.az.fitcline.viewmodels.DummyData;
import com.az.fitcline.views.customviews.NotificationView;
import com.az.fitcline.views.fragments.BaseFragment;

import javax.annotation.Nullable;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class MeFragment extends BaseFragment implements IFragmentBottomTag,IRightActionOption,IleftActionOption{
    private static final String TAG = "me";
    @BindView(R.id.tv_followers_count)
    TextView mTvFollowerCount;
    @BindView(R.id.tv_following_count)
    TextView mTvFollowingCount;
    @BindView(R.id.tv_username)
    TextView mTvUserName;
    @BindView(R.id.tv_user_id)
    TextView mTvUserId;
    @BindView(R.id.rv_messages)
    RecyclerView mRvMessages;
    @BindView(R.id.rv_notification)
    RecyclerView mRvNotifications;
    @BindView(R.id.nv_messasge)
    NotificationView mNvMessage;
    @BindView(R.id.nv_notification)
    NotificationView mNvNotifcation;


    public MeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_me, container, false);

    }

    @Override
    public void onViewCreated(View view, @android.support.annotation.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setDummyData();
    }

    public void setDummyData(){
        mTvFollowerCount.setText("7k");
        mTvFollowingCount.setText("126");
        mTvUserName.setText(UserModel.getRandomUser().getUserName());
        mTvUserId.setText("@uniqueId");
        mRvMessages.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvNotifications.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvNotifications.setNestedScrollingEnabled(false);
        mRvMessages.setNestedScrollingEnabled(false);
        mRvMessages.setAdapter(new MessageListAdapter(getContext(),DummyData.getDummyMessages()));
        mRvNotifications.setAdapter(new NotificationAdapter(getContext(),DummyData.getNotifications()));
        mNvMessage.setOptionAction(this::openMessages);
        mNvNotifcation.setOptionAction(this::openNotifications);
    }

    private void openNotifications() {
        FragmentUtility.withManager(getFragmentManager())
                .withAnimationType(FragmentAnimationType.GROW_FROM_BOTTOM)
                .addToBackStack(TAG)
                .replaceToFragment(new NotificationFragment());
    }

    private void openMessages() {
        FragmentUtility.withManager(getFragmentManager())
                .withAnimationType(FragmentAnimationType.GROW_FROM_BOTTOM)
                .addToBackStack(TAG)
                .replaceToFragment(new MessageListFragment());
    }

    @Nullable
    @Override
    public BottomMenuItems getMenuItem() {
        return BottomMenuItems.ME;
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return getString(R.string.title_me);
    }

    @Override
    public int getRightActionImageId() {
        return R.drawable.page;
    }

    @Override
    public void performRightAction() {

    }

    @Override
    public int getLeftActionImageId() {
        return R.drawable.vd_setting;
    }

    @Override
    public void performLeftAction() {
        FragmentUtility.withManager(getFragmentManager())
                .withAnimationType(FragmentAnimationType.GROW_FROM_BOTTOM)
                .addToBackStack(TAG)
                .replaceToFragment(new ProfileEditFragment());
    }
}

package com.az.fitcline.views.fragments.progressmodule;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.az.fitcline.R;
import com.az.fitcline.views.fragments.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditLogFragment extends BaseFragment {
    public static final String TAG = "edit_log_fragment";

    public EditLogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_log, container, false);
    }

    @Override
    public String getTagFragment() {
        return null;
    }

    @Override
    public String getTitle() {
        return null;
    }
}

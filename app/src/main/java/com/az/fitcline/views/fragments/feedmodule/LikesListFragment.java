package com.az.fitcline.views.fragments.feedmodule;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.adapters.UserFollowingAdapter;
import com.az.fitcline.viewmodels.DummyData;
import com.az.fitcline.views.fragments.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class LikesListFragment extends BaseFragment {
    private static final String TAG = "LikesFragment";

    private Unbinder bind;
    @BindView(R.id.rv_users)
    public RecyclerView mRvUsers;
    @BindView(R.id.tv_like_count)
    public TextView mTvLikeCount;


    public LikesListFragment() {
    }

    public static LikesListFragment newInstance() {
        LikesListFragment fragment = new LikesListFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_likes, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bind = ButterKnife.bind(this, view);
        mTvLikeCount.setText("1.8k peoples");
        setUserAdapter();
    }

    private void setUserAdapter() {
        List<Integer> followingIds = new ArrayList<>();
        followingIds.add(R.drawable.user_3);
        followingIds.add(R.drawable.user_4);
        mRvUsers.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvUsers.setAdapter(new UserFollowingAdapter(getContext(), DummyData.getmUsers(),followingIds));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        bind.unbind();
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return "";
    }
}

package com.az.fitcline.views.fragments.feedmodule;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.adapters.UserFollowingAdapter;
import com.az.fitcline.viewmodels.DummyData;
import com.az.fitcline.views.fragments.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserSearchFragment extends BaseFragment {
    private static final String TAG = "user_search";
    private Unbinder unbinder;
    @BindView(R.id.rv_users)
    RecyclerView mRvUsers;
    @BindView(R.id.tv_header)
    TextView mTvHeader;
    private static String mTitle;


    public UserSearchFragment() {
        // Required empty public constructor
    }

    public static UserSearchFragment newInstance(String title) {

        Bundle args = new Bundle();
        mTitle = title;
        UserSearchFragment fragment = new UserSearchFragment();
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user_search, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        mTvHeader.setText("Following 175");
        setUserAdapter();
    }

    private void setUserAdapter() {
        List<Integer> followingIds = new ArrayList<>();
        followingIds.add(R.drawable.user_3);
        followingIds.add(R.drawable.user_4);
        mRvUsers.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvUsers.setAdapter(new UserFollowingAdapter(getContext(), DummyData.getmUsers(),followingIds));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return mTitle;
    }
}

package com.az.fitcline.views.fragments.progressmodule;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.adapters.ProgressMonthlyWeightAdapter;
import com.az.fitcline.model.dataaccess.entities.ProgressMonthlyWeight;
import com.az.fitcline.model.dataaccess.enums.FragmentAnimationType;
import com.az.fitcline.model.utilities.FragmentUtility;
import com.az.fitcline.views.fragments.BaseFragment;

import butterknife.BindView;


public class MonthlyWeightFragment extends BaseFragment {
    public static final String TAG = "monthly_weight_fragment";

    @BindView(R.id.rv_add_monthly_weight_image)
    RecyclerView mRVMonthlyWeight;

    public MonthlyWeightFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_monthly_weight, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTypeAdapter();
    }

    private void setTypeAdapter() {

        mRVMonthlyWeight.setLayoutManager(new LinearLayoutManager(getContext()));
        mRVMonthlyWeight.setAdapter(new ProgressMonthlyWeightAdapter(getContext(),
                ProgressMonthlyWeight.getMonth(getContext()), progressModel -> {
            FragmentUtility.withManager(getFragmentManager())
                    .withAnimationType(FragmentAnimationType.SLIDE_FROM_RIGHT)
                    .addToBackStack(TAG)
                    .replaceToFragment(new EditMonthlyWeightFragment());
        }));
    }


    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return null;
    }
}

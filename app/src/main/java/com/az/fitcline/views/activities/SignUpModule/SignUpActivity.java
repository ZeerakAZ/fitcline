package com.az.fitcline.views.activities.SignUpModule;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.entities.UserModel;
import com.az.fitcline.model.dataaccess.validations.EmailValidation;
import com.az.fitcline.model.dataaccess.validations.EmptyValidation;
import com.az.fitcline.model.dataaccess.validations.Validation;
import com.az.fitcline.model.dataaccess.validations.Validator;
import com.az.fitcline.model.utilities.ActivityNavigationUtility;
import com.az.fitcline.model.utilities.ToastUtility;
import com.az.fitcline.viewmodels.UserPresenter;
import com.az.fitcline.views.activities.BaseActivity;
import com.az.fitcline.views.activities.MainActivity;
import com.az.fitcline.views.activities.SplashActivity;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpActivity extends BaseActivity implements UserPresenter.IUserListener {

    @BindView(R.id.vs_signUp)
    ViewSwitcher mVsSignUp;
    @BindView(R.id.et_email)
    EditText mEtEmail;
    @BindView(R.id.et_password)
    EditText mEtPassword;
    @BindView(R.id.et_first_name)
    EditText mEtFirstName;
    @BindView(R.id.et_last_name)
    EditText mEtLastName;
    @BindView(R.id.et_username)
    EditText mEtUserName;
    @BindView(R.id.btn_next)
    Button mBtnNext;
    @BindView(R.id.tv_terms)
    TextView tvTerms;
    @BindView(R.id.tv_facebook)
    TextView tvFacebook;

    private ArrayList<Validation> mValidationStep1;
    private ArrayList<Validation> mValidationStep2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        setValidations();
        setTerms();
        mVsSignUp.setInAnimation(this,R.anim.slide_in_from_right);
        mVsSignUp.setOutAnimation(this,R.anim.slide_out_to_left);
    }

    private void setValidations() {
        mValidationStep1 = new ArrayList<>();
        mValidationStep1.add(new EmptyValidation(getString(R.string.error_email), mEtEmail, getString(R.string.ph_error)));
        mValidationStep1.add(new EmailValidation(getString(R.string.error_invalid_email), mEtEmail, getString(R.string.ph_error)));
        mValidationStep1.add(new EmptyValidation(getString(R.string.error_password), mEtPassword, getString(R.string.ph_error)));
        mValidationStep2 = new ArrayList<>();
        mValidationStep2.add(new EmptyValidation(getString(R.string.error_full), mEtFirstName, getString(R.string.ph_error)));
        mValidationStep2.add(new EmptyValidation(getString(R.string.error_full), mEtLastName, getString(R.string.ph_error)));
        mValidationStep2.add(new EmptyValidation(getString(R.string.error_username), mEtUserName, getString(R.string.ph_error)));
    }

    @OnClick(R.id.btn_next)
    public void onClick() {
        switch (mVsSignUp.getCurrentView().getId()) {
            case R.id.ll_step_1:
                if (new Validator(this).validates(mValidationStep1)) {
                    mBtnNext.setText(R.string.ph_done);
                    mVsSignUp.showNext();
                   setBottomText();
                }
                break;
            case R.id.ll_step_2:
                if (new Validator(this).validates(mValidationStep2)) {
                    new UserPresenter(this,this).signUpUser(getLoginUser());
                }
                break;
        }

    }

    private UserModel getLoginUser() {
        return new UserModel()
                .setUserName(mEtUserName.getText().toString())
                .setEmail(mEtEmail.getText().toString())
                .setFirstName(mEtFirstName.getText().toString())
                .setLastName(mEtLastName.getText().toString())
                .setPassword(mEtPassword.getText().toString());

    }

    private void setBottomText(){
        tvFacebook.setVisibility(mVsSignUp.getCurrentView().getId() == R.id.ll_step_2?View.GONE:View.VISIBLE);
        tvTerms.setVisibility(mVsSignUp.getCurrentView().getId() == R.id.ll_step_2?View.VISIBLE:View.GONE);
    }

    @OnClick(R.id.tv_back)
    public void onBack() {
        onBackPressed();
    }


    @Override
    public void onBackPressed() {
        if (mVsSignUp.getCurrentView().getId() == R.id.ll_step_2) {
            mBtnNext.setText(R.string.ph_next);
            mVsSignUp.showPrevious();
            setBottomText();
        } else {
            super.onBackPressed();
        }
    }

    private void setTerms() {
        SpannableString string = new SpannableString(getString(R.string.message_term_and_condition));
        string.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.android_green))
                , 34, 48, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        string.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.android_green))
                , 53, 73, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvTerms.setText(string);
    }


    @Override
    public void userFetched(@NotNull UserModel user) {
        ToastUtility.showToastForLongTime(this, getString(R.string.message_welcome));
        ActivityNavigationUtility.navigateWith(SignUpActivity.this).
                setClearStack().
                navigateTo(MainActivity.class);
    }
}

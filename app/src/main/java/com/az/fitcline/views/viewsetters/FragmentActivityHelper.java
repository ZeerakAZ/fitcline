package com.az.fitcline.views.viewsetters;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.enums.BottomMenuItems;
import com.az.fitcline.model.dataaccess.enums.FragmentAnimationType;
import com.az.fitcline.model.dataaccess.interfaces.IFragmentBottomTag;
import com.az.fitcline.model.dataaccess.interfaces.IRightActionOption;
import com.az.fitcline.model.dataaccess.interfaces.IleftActionOption;
import com.az.fitcline.model.utilities.FragmentUtility;
import com.az.fitcline.views.activities.BaseActivity;
import com.az.fitcline.views.fragments.BaseFragment;
import com.az.fitcline.views.fragments.feedmodule.GroupsFragment;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Created by Zeera on 11/19/2017 bt ${File}
 */

public class FragmentActivityHelper{
    private IActivityContract iContract;
    private BaseActivity mHostingActivity;
    private ImageView mBottomMenuSelected = null;
    private BottomMenuItems mMenuSelectedItem;
    private TextView tvHeader;
    private ImageView backIcon;
    private ImageView rightIcon;
    private ImageView leftIcon;


    public FragmentActivityHelper(BaseActivity fragmentHostActivity) {

        if(!(fragmentHostActivity instanceof IActivityContract))
            throw new IllegalArgumentException("must implement contract ");
        this.iContract = (IActivityContract) fragmentHostActivity;
        this.mHostingActivity = fragmentHostActivity;
        tvHeader = fragmentHostActivity.findViewById(iContract.getHeaderTitleId());
        backIcon = fragmentHostActivity.findViewById(iContract.getHeaderBackId());
        rightIcon = fragmentHostActivity.findViewById(iContract.getRightIconId());
        leftIcon = fragmentHostActivity.findViewById(iContract.getLeftIcon());
        View viewById = fragmentHostActivity.findViewById(iContract.getBottomMenuParentId());
        leftIcon.setOnClickListener(this::leftIconClicked);
    }

    private void leftIconClicked(View v){
        BaseFragment currentFragment = FragmentUtility.getCurrentFragment(mHostingActivity,
                R.id.fl_fragment_container);
        FragmentUtility.withManager(mHostingActivity.getSupportFragmentManager())
                .withAnimationType(FragmentAnimationType.SLIDE_FROM_LEFT)
                .addToBackStack(currentFragment!=null?currentFragment.getTag():null)
                .replaceToFragment(new GroupsFragment());
    }


    public void fragmentUpdated(BaseFragment fragment) {
        if (fragment.getTitle()!=null) {
            tvHeader.setText(fragment.getTitle());
        }
        if(fragment instanceof IFragmentBottomTag){
            bottomMenuClicked(((IFragmentBottomTag) fragment).getMenuItem());
        }
        if(fragment instanceof IRightActionOption){
            setRightAction(((IRightActionOption) fragment));
        }else{
            rightIcon.setVisibility(View.GONE);
        }

        if(fragment instanceof IleftActionOption){
            leftIcon.setImageResource(((IleftActionOption) fragment).getLeftActionImageId());
            leftIcon.setOnClickListener(v -> ((IleftActionOption) fragment).performLeftAction());
        }else{
            leftIcon.setImageResource(R.drawable.menu);
            leftIcon.setOnClickListener(this::leftIconClicked);
        }
    }

    private void setRightAction(IRightActionOption fragment) {
        rightIcon.setVisibility(View.VISIBLE);
        rightIcon.setImageResource(fragment.getRightActionImageId());
        rightIcon.setOnClickListener(v -> {
            fragment.performRightAction();
        });
    }

    private void bottomMenuClicked(@Nullable BottomMenuItems item){
        if(item==null||(mMenuSelectedItem!=null&&item.equals(mMenuSelectedItem)))
            return;
        if(mBottomMenuSelected!=null){
            mBottomMenuSelected.setImageResource(mMenuSelectedItem.getUnSelectedResourceId());
        }
        mMenuSelectedItem  = item;
        mBottomMenuSelected = mHostingActivity.findViewById(item.getViewId());
        mBottomMenuSelected.setImageResource(mMenuSelectedItem.getSelectedResourceId());

    }


    public interface IActivityContract{
        int getHeaderTitleId();
        int getHeaderBackId();
        int getRightIconId();
        int getBottomMenuParentId();
        int getLeftIcon();
    }



}

package com.az.fitcline.views.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.adapters.BottomSheetOptionsAdapter;
import com.az.fitcline.model.dataaccess.entities.BottomSheetOptionModel;
import com.az.fitcline.model.dataaccess.interfaces.BottomOptionActions;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GenericBottomSheetDialog extends BottomSheetDialog {


    private final ArrayList<BottomSheetOptionModel> mOptions;
    private final IBottomOptionSelectionListener mOptionListener;
    @BindView(R.id.rv_options)
    RecyclerView mRvOptions;


    public GenericBottomSheetDialog(@NonNull Context context,
                                    ArrayList<BottomSheetOptionModel> options,
                                    IBottomOptionSelectionListener listener) {
        super(context,R.style.BottomSheetStyle);
        mOptions = options;
        mOptionListener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View sheetView = getLayoutInflater().inflate(R.layout.layout_bottom_sheet, null);
        setContentView(sheetView);
        ButterKnife.bind(this,sheetView);
        mRvOptions.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvOptions.setAdapter(new BottomSheetOptionsAdapter(getContext(), mOptions, actionId -> {
            dismiss();
            mOptionListener.performActionPlease(actionId);
        }));
    }


    @OnClick(R.id.tv_cancel)
    void dismissMe(){
        dismiss();
    }

    @FunctionalInterface
    public interface IBottomOptionSelectionListener{
        void performActionPlease(@BottomOptionActions int actionId);
    }


}

package com.az.fitcline.views.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.az.fitcline.model.dataaccess.interfaces.IFragmentUpdateListener;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Zeera on 10/29/2017 bt ${File}
 */

public abstract class BaseFragment extends Fragment {


    private Unbinder mUnBinder;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null && getActivity() instanceof IFragmentUpdateListener) {
            ((IFragmentUpdateListener) getActivity()).fragmentUpdated(this);
        }
        mUnBinder = ButterKnife.bind(this, view);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mUnBinder.unbind();
    }

    public abstract String getTagFragment();
    public abstract String getTitle();

}

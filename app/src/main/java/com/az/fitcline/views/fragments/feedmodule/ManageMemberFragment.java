package com.az.fitcline.views.fragments.feedmodule;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.adapters.GroupMemberAdapter;
import com.az.fitcline.model.dataaccess.entities.GroupMember;
import com.az.fitcline.model.dataaccess.entities.GroupModel;
import com.az.fitcline.views.fragments.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class ManageMemberFragment extends BaseFragment {

    private static final String TAG = "manageMember";
    private static final String ARG_GROUP = "arg_group";


    @BindView(R.id.rv_add_member)
    RecyclerView mRvAddMembers;

    private Unbinder unbinder;
    private GroupModel mGroup;

    public static ManageMemberFragment newInstance(GroupModel group) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_GROUP,group);
        ManageMemberFragment fragment = new ManageMemberFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public ManageMemberFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments()==null || !getArguments().containsKey(ARG_GROUP)) {
            throw new IllegalArgumentException("implementation not right dude");
        }
        mGroup = ((GroupModel) getArguments().getSerializable(ARG_GROUP));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_member, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        setGroupMemberAdapter();
    }

    private void setGroupMemberAdapter() {
        GroupMemberAdapter adapter = new GroupMemberAdapter(getContext(), mGroup.getmMembers(),
                new GroupMemberAdapter.IListViewOptions() {
                    @Override
                    public void onSettingClicked(GroupMember groupMember) {

                    }

                    @Override
                    public void onDeleteClicked(GroupMember groupMember) {

                    }
                });
        mRvAddMembers.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvAddMembers.setHasFixedSize(true);
        mRvAddMembers.setAdapter(adapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return "";
    }
}

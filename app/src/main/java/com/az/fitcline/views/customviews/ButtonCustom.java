package com.az.fitcline.views.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.az.fitcline.R;

/**
 * Created by Zeera on 12/31/2017 bt ${File}
 */

public class ButtonCustom extends LinearLayout {
    private int mButtonText;
    private int mDrawableLeft;

    public ButtonCustom(Context context) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.button_custtom,this);
    }

    public ButtonCustom(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }



    public ButtonCustom(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {

        LayoutInflater.from(getContext()).inflate(R.layout.button_custtom,this);
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs,R.styleable.ButtonCustom);
        mButtonText = typedArray.getResourceId(R.styleable.ButtonCustom_button_text,0);
        mDrawableLeft = typedArray.getResourceId(R.styleable.ButtonCustom_left_drawable,0);
        typedArray.recycle();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        TextView textView = findViewById(R.id.tv_btn);
        textView.setText(mButtonText);
        ImageView imageView = findViewById(R.id.iv_left);
        imageView.setImageResource(mDrawableLeft);
    }
}

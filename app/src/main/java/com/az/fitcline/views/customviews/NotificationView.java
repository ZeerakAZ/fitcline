package com.az.fitcline.views.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.az.fitcline.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zeera on 1/14/2018 bt ${File}
 */

public class NotificationView extends LinearLayout{
    @BindView(R.id.tv_new_count)
    TextView mTvNewCount;
    @BindView(R.id.tv_option_name)
    TextView mTvOptionName;
    @BindView(R.id.iv_notification_icon)
    ImageView mIvNotification;
    @BindView(R.id.tv_gap)
            TextView mTvGap;

    int optionNameId,optionImageId;

    public NotificationView(Context context) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.layout_notification_view,this);
    }

    public NotificationView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public NotificationView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        LayoutInflater.from(getContext()).inflate(R.layout.layout_notification_view,this);
        ButterKnife.bind(this);
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.NotificationView);

        optionNameId = typedArray.getResourceId(R.styleable.NotificationView_option_text,0);
        optionImageId = typedArray.getResourceId(R.styleable.NotificationView_notification_icon,0);
        typedArray.recycle();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        setGravity(Gravity.CENTER);
        if (optionNameId!=0) {
            mTvOptionName.setText(optionNameId);
        }
        if (optionImageId!=0) {
            mIvNotification.setImageResource(optionImageId);
        }
        setNewNotification(0);
    }


    public void setNewNotification(int newCount){
        mTvNewCount.setText(getContext().getString(R.string.ph_new_count,newCount));
        setColor(newCount);
    }

    private void setColor(int newCount) {
        int color = ContextCompat.getColor(getContext(),newCount==0? R.color.dimgray:
                R.color.android_green);
        mIvNotification.clearColorFilter();
        mIvNotification.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        mTvNewCount.setTextColor(color);
        mTvOptionName.setTextColor(color);
        mTvGap.setTextColor(color);
    }

    public void setOptionAction(IOptionListener optionAction){
        mTvOptionName.setOnClickListener(v -> {
            optionAction.performAction();
        });
    }

    @FunctionalInterface
    public interface IOptionListener{
        void performAction();
    }
}

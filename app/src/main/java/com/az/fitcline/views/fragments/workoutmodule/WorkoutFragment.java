package com.az.fitcline.views.fragments.workoutmodule;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.adapters.WorkoutListAdapter;
import com.az.fitcline.model.dataaccess.enums.BottomMenuItems;
import com.az.fitcline.model.dataaccess.enums.FragmentAnimationType;
import com.az.fitcline.model.dataaccess.interfaces.IFragmentBottomTag;
import com.az.fitcline.model.dataaccess.interfaces.IRightActionOption;
import com.az.fitcline.model.utilities.FragmentUtility;
import com.az.fitcline.viewmodels.DummyData;
import com.az.fitcline.views.fragments.BaseFragment;

import javax.annotation.Nullable;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class WorkoutFragment extends BaseFragment implements IFragmentBottomTag,IRightActionOption {

    private static final String TAG = "workout";

    @BindView(R.id.rv_workouts)
    RecyclerView mRvWorkouts;

    public WorkoutFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_workout, container, false);
    }

    @Override
    public void onViewCreated(View view, @android.support.annotation.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAdapter();
    }

    private void setAdapter() {
        mRvWorkouts.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvWorkouts.setAdapter(new WorkoutListAdapter(DummyData.getRandomWorkOuts(),getContext()));
    }

    @Nullable
    @Override
    public BottomMenuItems getMenuItem() {
        return BottomMenuItems.WORKOUT;
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return getString(R.string.title_workout);
    }

    @Override
    public int getRightActionImageId() {
        return R.drawable.add;
    }

    @Override
    public void performRightAction() {
        FragmentUtility.withManager(getFragmentManager())
                .withAnimationType(FragmentAnimationType.GROW_FROM_BOTTOM)
                .addToBackStack(getTagFragment())
                .replaceToFragment(new CreateWorkoutFragment());
    }
}

package com.az.fitcline.views.fragments.workoutmodule;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.az.fitcline.R;
import com.az.fitcline.views.fragments.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreateWorkoutPostFragment extends BaseFragment {
    private static final String TAG = "creatworkoutpost";

    public CreateWorkoutPostFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_create_workout_post, container, false);
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return null;
    }
}

package com.az.fitcline.views.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.az.fitcline.R;
import com.az.fitcline.model.essentials.service.FileUploadService;
import com.az.fitcline.model.essentials.service.FileUploadServiceKt;
import com.az.fitcline.model.utilities.FragmentUtility;
import com.az.fitcline.views.fragments.BaseFragment;

import org.jetbrains.annotations.NotNull;

import static com.az.fitcline.model.essentials.service.FileUploadServiceKt.KEY_IDENTIFIER;
import static com.az.fitcline.model.essentials.service.FileUploadServiceKt.KEY_PROGRESS;
import static com.az.fitcline.model.essentials.service.FileUploadServiceKt.KEY_SERVER_PATH;

public class BaseActivity extends AppCompatActivity implements FileUploadService.IUploadListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mUploadListener,
                new IntentFilter(FileUploadServiceKt.ACTION_FILE_UPLOAD));
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mUploadListener);
    }

    public BroadcastReceiver mUploadListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                int progress = extras.getInt(KEY_PROGRESS);
                String filePath = extras.getString(KEY_IDENTIFIER);
                String serverPath = null;
                if (progress == 100) {
                    serverPath = extras.getString(KEY_SERVER_PATH);
                }
                onUploadUpdated(filePath, progress, serverPath);
            }
        }
    };

    @Override
    public void onUploadUpdated(@NotNull String filePath, int progress,
                                @Nullable String serverPath) {
        BaseFragment currentFragment = FragmentUtility.getCurrentFragment(this, R.id.fl_fragment_container);
        if (currentFragment != null && currentFragment instanceof FileUploadService.IUploadListener)
            ((FileUploadService.IUploadListener) currentFragment).onUploadUpdated(filePath, progress, serverPath);
    }
}

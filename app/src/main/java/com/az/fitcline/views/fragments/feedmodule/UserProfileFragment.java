package com.az.fitcline.views.fragments.feedmodule;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.adapters.PostAdapter;
import com.az.fitcline.model.dataaccess.entities.BottomSheetOptionModel;
import com.az.fitcline.model.dataaccess.entities.UserModel;
import com.az.fitcline.model.dataaccess.enums.FragmentAnimationType;
import com.az.fitcline.model.dataaccess.interfaces.BottomOptionActions;
import com.az.fitcline.model.utilities.FragmentUtility;
import com.az.fitcline.viewmodels.DummyData;
import com.az.fitcline.views.dialogs.GenericBottomSheetDialog;
import com.az.fitcline.views.fragments.BaseFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserProfileFragment extends BaseFragment implements GenericBottomSheetDialog.IBottomOptionSelectionListener {
    private static final String TAG = "userProfileFragment";

    private Unbinder unbinder;
    @BindView(R.id.tv_followers_count)
    TextView mTvFollowerCount;
    @BindView(R.id.tv_following_count)
    TextView mTvFollowingCount;
    @BindView(R.id.tv_username)
    TextView mTvUserName;
    @BindView(R.id.tv_user_id)
    TextView mTvUserId;
    @BindView(R.id.tv_workout_posted)
    TextView mTvWorkoutPosted;
    @BindView(R.id.rv_posts)
    RecyclerView mRvPosts;



    public UserProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user_profile, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        setDummyData();
    }

    public void setDummyData(){
        mTvFollowerCount.setText("7k");
        mTvFollowingCount.setText("126");
        mTvUserName.setText(UserModel.getRandomUser().getUserName());
        mTvUserId.setText("@uniqueId");
        mTvWorkoutPosted.setText(getString(R.string.ph_workouts_posted, DummyData.getRandomWorkOuts().size()));
        mRvPosts.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvPosts.setAdapter(new PostAdapter(DummyData.getPostsRandom(),getContext()));

    }

    @OnClick(R.id.ll_work_out_posted)
    public void toWorkOutList(){
        FragmentUtility.withManager(getActivity().getSupportFragmentManager())
                .addToBackStack(getTagFragment())
                .withAnimationType(FragmentAnimationType.SLIDE_FROM_RIGHT)
                .replaceToFragment(new WorkoutListFragment());
    }

    @OnClick(R.id.iv_message)
    public void toMessageFragment(){
        FragmentUtility.withManager(getActivity().getSupportFragmentManager())
                .addToBackStack(getTagFragment())
                .withAnimationType(FragmentAnimationType.GROW_FROM_BOTTOM)
                .replaceToFragment(new MessageFragment());
    }

    @OnClick(R.id.iv_menu)
    public void showOptions(){
        ArrayList<BottomSheetOptionModel> bottomSheetOptionModels = new ArrayList<>();
        bottomSheetOptionModels.add(BottomOptionActions.ACTION_REPORT_SPAM);
        bottomSheetOptionModels.add(BottomOptionActions.ACTION_BLOCK_USER);
        new GenericBottomSheetDialog(getContext(), bottomSheetOptionModels, this).show();
    }

    @OnClick({R.id.ll_followers,R.id.ll_following})
    public void toUserFragment(View view){
        FragmentUtility.withManager(getActivity().getSupportFragmentManager())
                .addToBackStack(getTagFragment())
                .withAnimationType(FragmentAnimationType.GROW_FROM_BOTTOM)
                .replaceToFragment(UserSearchFragment.newInstance(getString(view.getId()==R.id.ll_followers?
                        R.string.title_follower:R.string.title_following)));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return "";
    }

    @Override
    public void performActionPlease(int actionId) {

    }
}

package com.az.fitcline.views.customviews.editCodeView;

/**
 * Created by Alexandr Grizhinku on 15/07/2017.
 * alexg.mobiledev@gmail.com
 */

public interface EditCodeWatcher {
    void onCodeChanged(String code);
}

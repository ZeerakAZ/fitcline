package com.az.fitcline.views.fragments.feedmodule

import android.app.Activity
import android.content.Intent
import android.opengl.Visibility
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import butterknife.OnClick
import com.az.fitcline.R
import com.az.fitcline.model.dataaccess.enums.BottomMenuItems
import com.az.fitcline.model.dataaccess.interfaces.IFragmentBottomTag
import com.az.fitcline.model.essentials.service.FileUploadService
import com.az.fitcline.model.utilities.ImageUtility
import com.az.fitcline.model.utilities.ToastUtility
import com.az.fitcline.viewmodels.ProfilePresenter
import com.az.fitcline.views.fragments.BaseFragment
import kotlinx.android.synthetic.main.fragment_profile.*

/**
 * A simple [Fragment] subclass.
 */
class ProfileFragment : BaseFragment(), IFragmentBottomTag,FileUploadService.IUploadListener,
        ProfilePresenter.IProfileCompleteListener {

    companion object {
        private const val TAG = "Profile_fragment"
        private const val REQUEST_CAMERA = 1
        private const val REQUEST_GALLERY = 2
    }

    private var mImageUtility: ImageUtility? = null
    private var mProfilePresenter:ProfilePresenter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mImageUtility = ImageUtility(this,iv_profile_pic, REQUEST_CAMERA, REQUEST_GALLERY)
        mProfilePresenter = ProfilePresenter(activity!!,this)

        iv_profile_pic_bg.setOnClickListener { mImageUtility?.checkCameraAndExternalStoragePermissions() }
        tv_done.setOnClickListener{proceed()}

    }


    override fun getTitle(): String {
        return getString(R.string.title_profile)
    }

    override fun getTagFragment(): String {
        return TAG
    }


    fun proceed() {
        mImageUtility?.imagePath?.run {
            mProfilePresenter?.uploadImage(this)
        }?:completeProfile()
    }

    private fun completeProfile(){

    }

    override fun getMenuItem(): BottomMenuItems? {
        return BottomMenuItems.FEED
    }

    override fun onUploadUpdated(filePath: String, progress: Int, serverPath: String?) {
        pb_image.visibility = View.VISIBLE
        pb_image.progress = progress
        serverPath?.run {
            ToastUtility.showToastForLongTime(context,"Upload image")
            //mProfilePresenter?.completeProfile(serverPath,)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            when(requestCode){
                REQUEST_GALLERY->{
                    mImageUtility?.convertAndSetImageToUI(data)
                }
                REQUEST_CAMERA ->{
                    mImageUtility?.setImageFromCameraResult(data)
                }
            }
        }
    }

    override fun profileUpdated(isSuccess: Boolean) {

    }
}

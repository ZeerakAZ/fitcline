package com.az.fitcline.views.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.adapters.MonthlyLogAdapter;
import com.az.fitcline.model.dataaccess.adapters.ProgressOverviewImageAdapter;
import com.az.fitcline.model.dataaccess.entities.ProgressMonthlyLog;
import com.az.fitcline.model.dataaccess.entities.ProgressOverview;
import com.az.fitcline.model.dataaccess.enums.BottomMenuItems;
import com.az.fitcline.model.dataaccess.enums.FragmentAnimationType;
import com.az.fitcline.model.dataaccess.interfaces.IFragmentBottomTag;
import com.az.fitcline.model.dataaccess.interfaces.IleftActionOption;
import com.az.fitcline.model.utilities.FragmentUtility;
import com.az.fitcline.views.fragments.progressmodule.AddProgressFragment;
import com.az.fitcline.views.fragments.progressmodule.MonthlyLogFragment;
import com.az.fitcline.views.fragments.progressmodule.MonthlyWeightFragment;

import javax.annotation.Nullable;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProgressFragment extends BaseFragment implements IFragmentBottomTag,IleftActionOption{
    private static final String TAG = "progress";
    @BindView(R.id.rv_OverviewImage)
    RecyclerView mRVImage;
    @BindView(R.id.rv_progress_log)
    RecyclerView mRvLog;

    public ProgressFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_progress, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTypeAdapter();
    }

    private void setTypeAdapter() {
        mRVImage.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, true));
        mRVImage.setAdapter(new ProgressOverviewImageAdapter(getContext(), ProgressOverview.getImageID(getContext())));

        mRvLog.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvLog.setAdapter(new MonthlyLogAdapter(getActivity()
                , ProgressMonthlyLog.getMonthlyLog(getContext(),true),null));
    }

    @Nullable
    @Override
    public BottomMenuItems getMenuItem() {
        return BottomMenuItems.PROGRESS;
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return getString(R.string.title_progress);
    }

    @Override
    public int getLeftActionImageId() {
        return R.drawable.progress_add;
    }

    @Override
    public void performLeftAction() {
        FragmentUtility.withManager(getFragmentManager())
                .withAnimationType(FragmentAnimationType.GROW_FROM_BOTTOM)
                .addToBackStack(TAG)
                .replaceToFragment(new AddProgressFragment());
    }

    @OnClick(R.id.ll_log)
    public void toLog(){
        FragmentUtility.withManager(getFragmentManager())
                .withAnimationType(FragmentAnimationType.SLIDE_FROM_RIGHT)
                .addToBackStack(TAG)
                .replaceToFragment(new MonthlyLogFragment());
    }

    @OnClick(R.id.ll_monthly_weight)
    public void toMonthlyWeight(){
        FragmentUtility.withManager(getFragmentManager())
                .withAnimationType(FragmentAnimationType.SLIDE_FROM_RIGHT)
                .addToBackStack(TAG)
                .replaceToFragment(new MonthlyWeightFragment());
    }
}

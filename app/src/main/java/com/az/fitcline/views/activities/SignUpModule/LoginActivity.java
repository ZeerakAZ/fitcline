package com.az.fitcline.views.activities.SignUpModule;

import android.os.Bundle;
import android.widget.EditText;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.entities.UserModel;
import com.az.fitcline.model.dataaccess.validations.EmptyValidation;
import com.az.fitcline.model.dataaccess.validations.Validation;
import com.az.fitcline.model.dataaccess.validations.Validator;
import com.az.fitcline.model.utilities.ActivityNavigationUtility;
import com.az.fitcline.model.utilities.ToastUtility;
import com.az.fitcline.viewmodels.UserPresenter;
import com.az.fitcline.views.activities.BaseActivity;
import com.az.fitcline.views.activities.MainActivity;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Zeera on 11/11/2017 bt ${File}
 */

public class LoginActivity extends BaseActivity implements UserPresenter.IUserListener {
    @BindView(R.id.et_username)
    EditText mEtUserName;
    @BindView(R.id.et_password)
    EditText mEtPassword;
    private ArrayList<Validation> mValidation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        setValidations();
    }

    private void setValidations() {
        mValidation = new ArrayList<>();
        mValidation.add(new EmptyValidation(getString(R.string.error_username), mEtUserName, getString(R.string.ph_error)));
        mValidation.add(new EmptyValidation(getString(R.string.error_password), mEtPassword, getString(R.string.ph_error)));
    }

    @OnClick(R.id.btn_signUp)
    public void toSignUp() {
        ActivityNavigationUtility.navigateWith(LoginActivity.this).
                navigateTo(SignUpActivity.class);
    }

    @OnClick(R.id.tv_forgot_details)
    public void toForgotDetails(){
        ActivityNavigationUtility.navigateWith(this)
        .navigateTo(ForgotPasswordActivity.class);
    }

    @OnClick(R.id.btn_login)
    public void logIn() {
        if (new Validator(this).validates(mValidation)) {
           new UserPresenter(this,this)
                   .loginUser(mEtUserName.getText().toString()
                   ,mEtPassword.getText().toString());
        }
    }

    @Override
    public void userFetched(@NotNull UserModel user) {
        ToastUtility.showToastForLongTime(this, getString(R.string.message_welcome));
        ActivityNavigationUtility.navigateWith(LoginActivity.this).
                setClearStack().
                navigateTo(MainActivity.class);
    }
}

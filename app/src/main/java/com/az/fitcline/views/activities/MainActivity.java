package com.az.fitcline.views.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.view.View;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.entities.BottomSheetOptionModel;
import com.az.fitcline.model.dataaccess.enums.FragmentAnimationType;
import com.az.fitcline.model.dataaccess.interfaces.BottomOptionActions;
import com.az.fitcline.model.dataaccess.interfaces.IFragmentUpdateListener;
import com.az.fitcline.model.utilities.FragmentUtility;
import com.az.fitcline.model.utilities.LogUtility;
import com.az.fitcline.views.dialogs.GenericBottomSheetDialog;
import com.az.fitcline.views.fragments.BaseFragment;
import com.az.fitcline.views.fragments.leaderboardmodule.LeaderboardBaseFragment;
import com.az.fitcline.views.fragments.memodule.MeFragment;
import com.az.fitcline.views.fragments.ProgressFragment;
import com.az.fitcline.views.fragments.workoutmodule.WorkoutFragment;
import com.az.fitcline.views.fragments.feedmodule.HomeFragment;
import com.az.fitcline.views.fragments.feedmodule.ProfileFragment;
import com.az.fitcline.views.viewsetters.FragmentActivityHelper;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements IFragmentUpdateListener,
        FragmentActivityHelper.IActivityContract {
    private FragmentActivityHelper fragmentActivityHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setBackgroundDrawable(null);///for minimize overdraw
        ButterKnife.bind(this);
        fragmentActivityHelper = new FragmentActivityHelper(this);

       /* FragmentUtility.withManager(getSupportFragmentManager())
                .replaceToFragment(new HomeFragment());*/
        addProfileFragment();
    }

    private void addProfileFragment() {
        FragmentUtility.withManager(getSupportFragmentManager())
                .intoContainerId(R.id.fl_fragment_container)
                .replaceToFragment(new ProfileFragment());

    }

    @Override
    public void fragmentUpdated(BaseFragment fragment) {
        fragmentActivityHelper.fragmentUpdated(fragment);
    }

    @OnClick({R.id.iv_bottom_feed, R.id.iv_bottom_leaderboard, R.id.iv_bottom_workout,
            R.id.iv_bottom_progress, R.id.iv_bottom_me})
    public void bottomMenuClicked(View view) {
        BaseFragment fragment = null;
        switch (view.getId()) {

            case R.id.iv_bottom_feed:
                fragment = new HomeFragment();
                break;
            case R.id.iv_bottom_leaderboard:
                fragment = new LeaderboardBaseFragment();
                break;
            case R.id.iv_bottom_workout:
                fragment = new WorkoutFragment();
                break;
            case R.id.iv_bottom_progress:

                fragment = new ProgressFragment();
                break;
            case R.id.iv_bottom_me:
                fragment = new MeFragment();
                break;
        }
        if (fragment != null) {
            BaseFragment currentFragment = FragmentUtility.getCurrentFragment(this, R.id.fl_fragment_container);

            if(getSupportFragmentManager().findFragmentByTag(fragment.getTagFragment())!=null)
                fragment = ((BaseFragment) getSupportFragmentManager().findFragmentByTag(fragment.getTagFragment()));

            if(currentFragment!=null&& currentFragment.getClass().getName().equals(fragment.getClass().getName()))
                return;



            if (currentFragment != null && !(currentFragment instanceof ProfileFragment)){
                FragmentUtility.withManager(getSupportFragmentManager())
                        .addToBackStack(currentFragment.getTagFragment())
                        .withAnimationType(FragmentAnimationType.FADE_IN)
                        .replaceToFragment(fragment);
            }
            else
                FragmentUtility.withManager(getSupportFragmentManager())
                        .withAnimationType(FragmentAnimationType.FADE_IN)
                        .replaceToFragment(fragment);
            getSupportFragmentManager().popBackStack(fragment.getTagFragment(),
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
            new Handler().postDelayed(() -> LogUtility.debugLog(""),1000);

        }
    }

    @OnClick(R.id.iv_main_drawer)
    public void rightButtonClick() {
        ArrayList<BottomSheetOptionModel> bottomSheetOptionModels = new ArrayList<>();
        bottomSheetOptionModels.add(BottomOptionActions.ACTION_DELETE_COMMENT);
        bottomSheetOptionModels.add(BottomOptionActions.ACTION_EDIT_COMMENT);
        bottomSheetOptionModels.add(BottomOptionActions.ACTION_REPORT_SPAM);
        new GenericBottomSheetDialog(this, bottomSheetOptionModels, actionId -> {
        }).show();
    }

    @Override
    public int getHeaderTitleId() {
        return R.id.tv_main_title_top;
    }

    @Override
    public int getHeaderBackId() {
        return R.id.iv_main_back;
    }

    @Override
    public int getRightIconId() {
        return R.id.iv_main_right_option;
    }

    @Override
    public int getBottomMenuParentId() {
        return R.id.cl_bottom;
    }

    @Override
    public int getLeftIcon() {
        return R.id.iv_main_drawer;
    }
}

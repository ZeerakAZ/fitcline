package com.az.fitcline.views.fragments.feedmodule;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.adapters.ImagesAdapter;
import com.az.fitcline.model.dataaccess.entities.ImageModel;
import com.az.fitcline.views.fragments.BaseFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostingFragment extends BaseFragment {
    public static final String TAG = "postingFragment";
    @BindView(R.id.rv_images)
    RecyclerView mRvImages;
    @BindView(R.id.iv_camera)
    ImageView mIvCamera;

    private Unbinder unbinder;

    public PostingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_posting, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        mIvCamera.setSelected(true);
        setImagesAdapter();
    }

    private void setImagesAdapter() {
        mRvImages.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));
        mRvImages.setAdapter(new ImagesAdapter(ImageModel.getImagesData(),getContext()));

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return getString(R.string.title_home);
    }

    @OnClick(R.id.iv_close)
    public void closeMe(){
        getActivity().onBackPressed();
    }
}

package com.az.fitcline.views.fragments.feedmodule;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.adapters.MessageAdapter;
import com.az.fitcline.model.dataaccess.entities.MessageModel;
import com.az.fitcline.model.dataaccess.entities.UserModel;
import com.az.fitcline.viewmodels.DummyData;
import com.az.fitcline.views.fragments.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class MessageFragment extends BaseFragment {
    private static final String TAG = "message_fragment";

    private Unbinder unbinder;
    @BindView(R.id.rv_messages)
    RecyclerView mRvMessages;
    @BindView(R.id.et_message)
    EditText mEtMessage;
    @BindView(R.id.tv_username)
    TextView mTvUserName;

    private MessageAdapter mMessageAdapter;

    public MessageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_message, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        setMessageAdapter();
        mTvUserName.setText(UserModel.getRandomUser().getUserName());
    }

    private void setMessageAdapter() {
        mMessageAdapter = new MessageAdapter(getContext(),
                DummyData.getDummyMessages());
        mRvMessages.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvMessages.setAdapter(mMessageAdapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @OnClick(R.id.iv_send)
    public void sendMessga() {
        if (!mEtMessage.getText().toString().trim().isEmpty())
            updateLocalList(mEtMessage.getText().toString());
        mEtMessage.setText("");
    }

    private void updateLocalList(String message) {
        if (mMessageAdapter != null) {
            MessageModel messageModel = new MessageModel(message, 1, true,UserModel.getRandomUser());
            mMessageAdapter.addMessage(messageModel);
            mRvMessages.getLayoutManager().scrollToPosition(DummyData.getDummyMessages().size()-1);
        }
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return "";
    }
}

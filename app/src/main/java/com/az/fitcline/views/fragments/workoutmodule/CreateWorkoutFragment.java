package com.az.fitcline.views.fragments.workoutmodule;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.adapters.WorkoutOptionAdapter;
import com.az.fitcline.model.dataaccess.entities.WorkOutOptionModel;
import com.az.fitcline.model.dataaccess.enums.FragmentAnimationType;
import com.az.fitcline.model.utilities.FragmentUtility;
import com.az.fitcline.views.fragments.BaseFragment;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreateWorkoutFragment extends BaseFragment {
    private static final String TAG = "createworkout";

    @BindView(R.id.rv_options)
    RecyclerView mRvOptions;

    public CreateWorkoutFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_create_workout, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRvOptions.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvOptions.setAdapter(new WorkoutOptionAdapter(getContext(),WorkOutOptionModel.getOptions()));
    }

    @OnClick(R.id.btn_next)
    void proceed(){
        FragmentUtility.withManager(getFragmentManager())
                .withAnimationType(FragmentAnimationType.SLIDE_FROM_RIGHT)
                .addToBackStack(getTagFragment())
                .replaceToFragment(new CreateWorkoutPostFragment());
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return null;
    }
}

package com.az.fitcline.views.fragments.feedmodule;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.entities.GroupModel;
import com.az.fitcline.model.dataaccess.enums.FragmentAnimationType;
import com.az.fitcline.model.utilities.FragmentUtility;
import com.az.fitcline.views.fragments.BaseFragment;

import java.security.acl.Group;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class GroupSettingFragment extends BaseFragment {
    private static final String TAG = "groupSettingFragment";
    private static final String ARG_GROUP = "arg_group";
    private GroupModel mGroup;

    @BindView(R.id.tv_group_tag)
    TextView mTvGroupTag;
    @BindView(R.id.rb_private)
    RadioButton mRbPrivate;
    @BindView(R.id.rb_public)
    RadioButton mRbPublic;


    public static GroupSettingFragment newInstance(GroupModel group) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_GROUP,group);

        GroupSettingFragment fragment = new GroupSettingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments()==null || !getArguments().containsKey(ARG_GROUP)) {
            throw new IllegalArgumentException("implementation not right dude");
        }
        mGroup = ((GroupModel) getArguments().getSerializable(ARG_GROUP));
    }

    public GroupSettingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setGroupData();
    }

    private void setGroupData() {
        if (mGroup != null) {
            mTvGroupTag.setText(mGroup.getGroupTag());
            mRbPrivate.setChecked(mGroup.isPrivate());

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_group_setting, container, false);
    }

    @OnClick(R.id.ll_mange_member)
    public void toManageMembersScreen(){
        FragmentUtility.withManager(getFragmentManager())
                .addToBackStack(getTagFragment())
                .withAnimationType(FragmentAnimationType.GROW_FROM_BOTTOM)
                .replaceToFragment(ManageMemberFragment.newInstance(mGroup));
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    @Override
    public String getTitle() {
        return "";
    }
}

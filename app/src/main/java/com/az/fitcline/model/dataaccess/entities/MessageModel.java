package com.az.fitcline.model.dataaccess.entities;

/**
 * Created by Zeera on 12/17/2017 bt ${File}
 */

public class MessageModel {
    private String messageText;
    private int messageTime;
    private boolean isMe;
    private UserModel user;

    private static String[] messgaesText = new String[]{
            "Hello", "We rock today", "Super tired", "Need your help mate",
            "we should do this more often", "You are one slimy bastard", "You need to chill",
            "Lets go somewhere", "let skip today work out ", "Once upon time was one lad who skip leg" +
            " day .the days goes by and his leg become more like chicken then he dies .so sad.Moral is that never skip leg days you lazy shit"
    };

    public MessageModel(String messageText, int messageTime, boolean isMe,UserModel userModel) {
        this.messageText = messageText;
        this.messageTime = messageTime;
        this.isMe = isMe;
        this.user = userModel;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public int getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(int messageTime) {
        this.messageTime = messageTime;
    }

    public boolean isMe() {
        return isMe;
    }

    public void setMe(boolean me) {
        isMe = me;
    }

    public static MessageModel getRandomMessage(UserModel userModel) {

        int rand = ((int) (Math.random() * (messgaesText.length-1)));
        return new MessageModel(messgaesText[rand], rand, rand % 2 == 0,userModel);
    }

    public UserModel getUser() {
        return user;
    }
}

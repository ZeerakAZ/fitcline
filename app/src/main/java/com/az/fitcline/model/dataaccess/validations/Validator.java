package com.az.fitcline.model.dataaccess.validations;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by Administrator on 5/12/2017.
 */

public class Validator {

    public Validator(Context context){
    }

    public boolean validates(ArrayList<Validation> validations) {
        boolean isValidated = false;
        for (Validation validation : validations) {
            isValidated = validation.passes();
            if (!isValidated){
                return false;
            }
        }
        return true;
    }

    public boolean validates(Validation...validation) {
        for (Validation validation1 : validation) {
            if(!validation1.passes()){
                return false;
            }
        }
        return true;

    }

}

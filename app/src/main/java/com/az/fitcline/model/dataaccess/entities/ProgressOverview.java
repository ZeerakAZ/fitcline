package com.az.fitcline.model.dataaccess.entities;

import android.content.Context;

import com.az.fitcline.R;

import java.util.ArrayList;

/**
 * Created by Wajahat on 25/02/2018.
 */

public class ProgressOverview{

    private static int[] MONTH_IMAGE = new int[] {
            R.drawable.progress_monthly_weight1,R.drawable.progress_monthly_weight2,R.drawable.progress_monthly_weight2,R.drawable.progress_monthly_weight1
    };

    private static int[] MONTH_WEIGHT = new int[]{
            R.string.progress_month_aug,R.string.progress_month_jun,R.string.progress_month_jul,R.string.progress_month_sept
    };

    private String month;
    private String weight;

    private int imageMonth;

    public void setImageMonth(int imageMonth){
        this.imageMonth=imageMonth;
    }

    public static ArrayList<ProgressOverview> getImageID(Context context){
        ArrayList<ProgressOverview> images = new ArrayList <>();
        for (int i : MONTH_IMAGE) {
            ProgressOverview WeightImage = new ProgressOverview();
            WeightImage.setImageMonth(i);
            images.add(WeightImage);
        }
        return images;
    }


    public int getImageMonth(){
        return imageMonth;
    }
}

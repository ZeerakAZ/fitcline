package com.az.fitcline.model.dataaccess.adapters;

import android.content.Context;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.entities.WorkoutImage;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Wajahat on 23/02/2018.
 */

public class WeightImageAdapter extends RecyclerView.Adapter<WeightImageAdapter.ViewHolder>{

    private ArrayList<WorkoutImage> mImage;
    private Context mContext;
    private int mSelectedItem;

    public WeightImageAdapter(Context mContext, ArrayList<WorkoutImage> mImage){
        this.mImage = mImage;
        this.mContext = mContext;
        this.mSelectedItem = mImage.get(0).getImageName();
    }


    @Override
    public WeightImageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);

        return new WeightImageAdapter.ViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.row_monthly_picture;
    }

    public int getItemCount() {
        return mImage == null ? 0 : mImage.size();
    }

    public void onBindViewHolder(WeightImageAdapter.ViewHolder holder, int position) {
        holder.bindData(mImage.get(position));

    }
    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_weight_pic)
        ImageView mWeightPic;
        @BindView(R.id.iv_delete_weight_pic)
        ImageView mDeletePic;


        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindData(WorkoutImage workoutImage){

            mWeightPic.setImageResource(workoutImage.getImageName());
        }


    }



}

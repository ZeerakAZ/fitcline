package com.az.fitcline.model.dataaccess.network;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;


import com.az.fitcline.R;
import com.az.fitcline.model.utilities.LogUtility;
import com.az.fitcline.views.dialogs.ErrorDialog;
import com.az.fitcline.views.dialogs.LoadingDialog;


import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CallApi {

    private IResponse iResponse;
    private IResponseSuccessListener mOverrideSuccessCallback;
    private IResponseFailureListener mOverrideFailureCallback;
    private ErrorDialog errorDialog;
    private LoadingDialog loadingDialog;
    private Context context;
    private String errorMessage;
    private String loadingMessage;

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getLoadingMessage() {
        return loadingMessage;
    }

    public CallApi(Context context, IResponse Iresponse) {
        this.iResponse = Iresponse;
        if(context instanceof Activity){
            this.errorDialog = new ErrorDialog(context);
            this.loadingDialog = new LoadingDialog(context);
        }

        this.context = context;
    }

    public void callService(Call<ResponseBody> call, final String endPoint) {
        if (getLoadingMessage() != null)
            showLoadingDialog(true, getLoadingMessage());
        LogUtility.debugLog("calling api", endPoint);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                try {
                    showLoadingDialog(false, "");
                    if (response.isSuccessful() && response.body() != null) {
                        String responseBody = response.body().string();
                        if(mOverrideSuccessCallback!=null)
                            mOverrideSuccessCallback.onSuccess(responseBody,endPoint);
                        else
                            iResponse.onSuccess(responseBody, endPoint);
                        showLoadingDialog(false, "");
                        LogUtility.debugLog("Server response", responseBody);
                    } else if (response.code() == 404)
                        showErrorDialog(true, context.getString(R.string.not_found));
                    else if (response.code() == 403) {
                        showErrorDialog(true, "Access Denied");
                    } else {
                        String error = response.errorBody().string();
                        JSONObject jError = new JSONObject(error);
                        String errorMessage = jError.getString("error");
                        iResponse.onError(errorMessage, endPoint);
                        LogUtility.debugLog("Server error", error);
                        showErrorDialog(true, errorMessage);
                    }
                } catch (Exception e) {
                    showLoadingDialog(false, "");
                    showErrorDialog(true, e.getMessage());
                    e.printStackTrace();
                    if (response.code() == 500) {
                        showErrorDialog(true, context.getString(R.string.internal_server_error));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                iResponse.onFailed(t, endPoint);
                mOverrideFailureCallback.onFailure(t,endPoint);
                LogUtility.debugLog("exception error", t.toString());
                showLoadingDialog(false, "");
                if (t instanceof SocketTimeoutException) {
                    showErrorDialog(true, context.getString(R.string.server_not_available));
                } else if (t instanceof IOException) {
                    showErrorDialog(true, context.getString(R.string.no_internet));
                } else {
                    showErrorDialog(true, t.getMessage());
                    t.printStackTrace();
                }
            }
        });
    }

    public CallApi setmOverrideFailureCallback(IResponseFailureListener mOverrideFailureCallback) {
        this.mOverrideFailureCallback = mOverrideFailureCallback;
        return this;
    }

    public CallApi setmOverrideSuccessCallback(IResponseSuccessListener mOverrideSuccessCallback) {
        this.mOverrideSuccessCallback = mOverrideSuccessCallback;
        return this;
    }

    private void showErrorDialog(boolean isShow, String errorMessage) {
        if(errorDialog==null)
            return;

        if (isShow && !errorDialog.isShowing()) {
            errorDialog.show();
            errorDialog.setErrorText(errorMessage);
        } else
            errorDialog.dismiss();
    }

    private void showLoadingDialog(boolean isShow, String loadingMessage) {
        if(loadingDialog==null)
            return;

        if (isShow && !loadingDialog.isShowing()) {
            loadingDialog.show();
            loadingDialog.setMessage(loadingMessage);
        } else {
            loadingDialog.dismiss();
        }
    }

    public void setErrorMessage(String message) {
        errorMessage = message;
    }

    public void setLoadingMessage(String message) {
        loadingMessage = message;
    }

    @FunctionalInterface
    public interface IResponseSuccessListener{
        void onSuccess(String body,String endPoint);
    }

    @FunctionalInterface
    public interface IResponseFailureListener{
        void onFailure(Throwable e,String endPoint);
    }
}
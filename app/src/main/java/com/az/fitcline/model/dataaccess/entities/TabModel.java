package com.az.fitcline.model.dataaccess.entities;

import com.az.fitcline.views.fragments.BaseFragment;

/**
 * Created by Zeera on 1/6/2018 bt ${File}
 */

public class TabModel {
    private BaseFragment fragment;
    private String mTabName;

    public TabModel(BaseFragment fragment, String mTabName) {
        this.fragment = fragment;
        this.mTabName = mTabName;
    }

    public BaseFragment getFragment() {
        return fragment;
    }

    public String getmTabName() {
        return mTabName;
    }
}

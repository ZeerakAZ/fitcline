package com.az.fitcline.model.dataaccess.enums;

import com.az.fitcline.R;

/**
 * Created by Zeera on 11/19/2017 bt ${File}
 */

public enum  BottomMenuItems {

    FEED(R.drawable.icon_feed,R.drawable.icon_feed_selected,R.id.iv_bottom_feed),
    WORKOUT(R.drawable.add,R.drawable.icon_workout_selected,R.id.iv_bottom_workout),
    LEADER_BOARD(R.drawable.icon_leaderboard,R.drawable.add,R.id.iv_bottom_leaderboard),
    PROGRESS(R.drawable.add,R.drawable.icon_progress_selected,R.id.iv_bottom_progress),
    ME(R.drawable.icon_me,R.drawable.add,R.id.iv_bottom_me);


    BottomMenuItems( int unSelectedResourceId, int selectedResourceId,int viewId) {
        this.viewId = viewId;
        this.selectedResourceId = selectedResourceId;
        this.unSelectedResourceId = unSelectedResourceId;
    }

    public int getSelectedResourceId() {
        return selectedResourceId;
    }

    public int getUnSelectedResourceId() {
        return unSelectedResourceId;
    }

    public int getViewId() {
        return viewId;
    }

    private int viewId;
    private int selectedResourceId;
    private int unSelectedResourceId;
}

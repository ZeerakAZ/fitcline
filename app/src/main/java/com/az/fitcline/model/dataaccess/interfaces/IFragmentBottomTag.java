package com.az.fitcline.model.dataaccess.interfaces;

import com.az.fitcline.model.dataaccess.enums.BottomMenuItems;

import javax.annotation.Nullable;

/**
 * Created by Zeera on 11/19/2017 bt ${File}
 */

public interface IFragmentBottomTag {

    default boolean showBottomSheet(){
        return true;
    }

    @Nullable
    BottomMenuItems getMenuItem();

}

package com.az.fitcline.model.dataaccess.enums;

import android.content.Context;

import com.az.fitcline.R;

/**
 * Created by Zeera on 12/17/2017 bt ${File}
 */

public enum  WorkDifficulty {
    BEGINNER(R.string.difficulty_beginner,0),
    INTERMEDIATE(R.string.difficulty_intermediate,1),
    ADVANCE(R.string.difficulty_advance,2);

    WorkDifficulty(int nameId, int level) {
        this.nameId = nameId;
        this.level = level;
    }

    private int nameId;
    private int level;

    public String getName(Context context) {
        return context.getString(nameId);
    }
}

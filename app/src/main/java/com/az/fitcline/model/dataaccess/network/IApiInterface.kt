package com.az.fitcline.model.dataaccess.network

import com.az.fitcline.model.dataaccess.entities.UserModel
import okhttp3.MultipartBody
import okhttp3.RequestBody

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by Zeera on 7/3/2017 bt ${File}
 */

interface IApiInterface {
    @FormUrlEncoded
    @POST(APIsEndPoints.CREATE_SESSION)
    fun createSession(@Field("platform") platform: String,
                      @Field("cultureCode") cultureCode: String, @Field("latitude") lat: Long? = null,
                      @Field("longitude") lng: Long? = null,
                      @Field("deviceToken") deviceToken: String? = null): Call<ResponseBody>

    @FormUrlEncoded
    @POST(APIsEndPoints.LOGIN)
    fun login(@Field("Token") token: String, @Field("Email") email: String,
              @Field("Password") password: String): Call<ResponseBody>

    @POST(APIsEndPoints.SIGNUP)
    fun signUp(@Body userModel: UserModel): Call<ResponseBody>

    @FormUrlEncoded
    @POST(APIsEndPoints.REQUEST_PASSWORD_RECOVERY)
    fun requestForgotPassword(@Field("Token") token: String, @Field("Email") email: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST(APIsEndPoints.VERIFY_PASSWORD_RECOVERY)
    fun verifyPasswordRecovery(@Field("Token") token: String, @Field("Code") code: String,
                               @Field("UserId") userId: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST(APIsEndPoints.CHANGE_PASSWORD)
    fun chagePassword(@Field("Token") token: String, @Field("Code") code: String,
                      @Field("UserId") userId: String, @Field("Password") password: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST(APIsEndPoints.COMPLETE_PROFILE)
    fun completeProfile(@Field("PictureId") pictureId: String?, @Field("Gender") gender: Int,
                        @Field("BirthDay") birthday: Long, @Field("Units") unit: Int): Call<ResponseBody>

    @Multipart
    @POST(APIsEndPoints.UPLOAD_FILE)
    fun uploadFile(@Part("Token") token: RequestBody,
                            @Part filePart: MultipartBody.Part): Call<ResponseBody>
}

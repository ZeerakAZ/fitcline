package com.az.fitcline.model.dataaccess.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.az.fitcline.model.dataaccess.database.converter.DateConverter;
import com.az.fitcline.model.dataaccess.database.dao.UserDao;
import com.az.fitcline.model.dataaccess.database.entity.User;


/**
 * Created by Philippe on 02/03/2018.
 */

@Database(entities = {User.class}, version = 1)
@TypeConverters(DateConverter.class)
public abstract class MyDatabase extends RoomDatabase {

    // --- SINGLETON ---
    private static volatile MyDatabase INSTANCE;

    // --- DAO ---
    public abstract UserDao userDao();
}

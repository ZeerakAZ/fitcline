package com.az.fitcline.model.dataaccess.network;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Zeera on 6/28/2017 bt ${File}
 */

public class ApiClient {
    private static Retrofit retrofit = null;

    public static final String BASE_URL="http://72.22.77.41/FitclineDev/";

    private static Retrofit retrofitFileUpload = null;

    private final static OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .build();

    private static Retrofit getClient() {
        if (retrofit == null) {

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public static Retrofit getFileUploadClient(){
        if (retrofitFileUpload == null) {
            OkHttpClient.Builder builder = new OkHttpClient.Builder()
                    .readTimeout(300, TimeUnit.SECONDS)
                    .connectTimeout(300, TimeUnit.SECONDS)
                    .writeTimeout(300,TimeUnit.SECONDS);

            retrofitFileUpload = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(builder.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofitFileUpload;
    }


    public static IApiInterface getApiClient(){
        return getClient().create(IApiInterface.class);
    }
}

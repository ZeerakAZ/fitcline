package com.az.fitcline.model.dataaccess.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.entities.WorkoutType;
import com.az.fitcline.model.dataaccess.enums.ExerciseType;
import com.az.fitcline.model.dataaccess.enums.ProgressType;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zeera on 3/4/2018 bt ${File}
 */

public class ExerciseAdapter extends RecyclerView.Adapter<ExerciseAdapter.ViewHolder> {


    private final ITypeListener mListener;
    private final ExerciseType[] mTypes;
    private ExerciseType mSelected;
    private Context mContext;

    public ExerciseAdapter(Context mContext, ITypeListener listener) {
        mTypes = ExerciseType.values();
        this.mContext = mContext;
        mListener = listener;
        mSelected = ExerciseType.BENCH;
    }

    @Override
    public ExerciseAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return new ExerciseAdapter.ViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.row_progress_strength;
    }

    @Override
    public int getItemCount() {
        return mTypes == null ? 0 : mTypes.length;
    }

    @Override
    public void onBindViewHolder(ExerciseAdapter.ViewHolder holder, int position) {
        holder.bindData(mTypes[position]);

    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_workout_type)
        TextView mRvWorkoutType;
        @BindView(R.id.iv_arrow)
        ImageView mIvArrow;
        @BindView(R.id.divider)
        View mDivider;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindData(ExerciseType type) {
            mRvWorkoutType.setText(type.getName(mContext));
            mRvWorkoutType.setOnClickListener(view -> {
                mSelected = type;
                notifyDataSetChanged();
                if (mListener != null) {
                    mListener.typeUpdated(type);
                }
            });

            if (type.equals(mSelected)) {
                mRvWorkoutType.setTextColor(ContextCompat.getColor(mContext, R.color.apple_green));
                mIvArrow.setVisibility(View.VISIBLE);
            } else {
                mRvWorkoutType.setTextColor(ContextCompat.getColor(mContext, R.color.dimgray));
                mIvArrow.setVisibility(View.INVISIBLE);
            }

            mDivider.setVisibility(getAdapterPosition() == getItemCount() - 1 ? View.GONE : View.VISIBLE);
        }
    }

    public interface ITypeListener {
        void typeUpdated(ExerciseType type);
    }

}
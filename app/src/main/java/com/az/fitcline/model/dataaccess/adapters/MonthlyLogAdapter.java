package com.az.fitcline.model.dataaccess.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.entities.ProgressMonthlyLog;
import com.az.fitcline.model.dataaccess.enums.FragmentAnimationType;
import com.az.fitcline.model.utilities.FragmentUtility;
import com.az.fitcline.views.activities.BaseActivity;
import com.az.fitcline.views.fragments.progressmodule.MonthlyLogFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Wajahat ${FILE} on 27/02/2018 bt ${File}
 */

public class MonthlyLogAdapter extends RecyclerView.Adapter<MonthlyLogAdapter.ViewHolder> {

    private final IEditLogListener mlistner;
    private ArrayList<ProgressMonthlyLog> mLogs;
    private Activity mContext;

    public MonthlyLogAdapter(Activity mContext, ArrayList<ProgressMonthlyLog> mLogs,IEditLogListener listener) {
        this.mContext = mContext;
        this.mLogs = mLogs;
        mlistner = listener;
    }

    @Override
    public MonthlyLogAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return new MonthlyLogAdapter.ViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.row_progress_log;
    }

    @Override
    public void onBindViewHolder(MonthlyLogAdapter.ViewHolder holder, int position) {
        holder.bindData(mLogs.get(position),
                position==0?null:mLogs.get(position-1));
    }

    @Override
    public int getItemCount() {
        return mLogs == null ? 0 : mLogs.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_header_month)
        TextView mTvMonth;
        @BindView(R.id.tv_exercise)
        TextView mTvExercise;
        @BindView(R.id.tv_weight)
        TextView mTvWeight;
        @BindView(R.id.tv_date)
        TextView mTvDate;
        @BindView(R.id.rl_log)
        RelativeLayout mRlLog;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        public void bindData(ProgressMonthlyLog monthlyLog,@Nullable ProgressMonthlyLog lastEntry ) {
            mTvMonth.setText(monthlyLog.getMonth());
            mTvExercise.setText(monthlyLog.getExercise());
            mTvWeight.setText(mContext.getString(R.string.value_pound,monthlyLog.getWeight()));
            mTvDate.setText("27/9");
            if(lastEntry!=null){
                mTvMonth.setVisibility(lastEntry.getMonth().
                        equalsIgnoreCase(monthlyLog.getMonth())?View.GONE:View.VISIBLE);
            }else{
                mTvMonth.setVisibility(View.VISIBLE);
            }
            mRlLog.setOnClickListener(this::toEditLog);



        }

        private void toEditLog(View view) {
            if (mlistner != null) {
                mlistner.logClicked(mLogs.get(getAdapterPosition()));
            }
        }


    }

    public interface IEditLogListener{
        void logClicked(ProgressMonthlyLog log);
    }
}

package com.az.fitcline.model.dataaccess.interfaces;

import android.support.annotation.DrawableRes;

/**
 * Created by Zeera on 1/28/2018 bt ${File}
 */

public interface IleftActionOption {
    @DrawableRes
    int getLeftActionImageId();
    void performLeftAction();
}

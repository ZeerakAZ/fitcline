package com.az.fitcline.model.dataaccess.entities;

import android.content.Context;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.enums.ProgressType;

import java.util.ArrayList;

/**
 * Created by Wajahat on 18/02/2018.
 */

public class WorkoutType {

    private static int[] TYPE = new int[]{
            R.string.progress_type_weight_pic, R.string.progress_type_strength
    };



    private String name;
    @ProgressType
    private int id;

    public WorkoutType setName(String name) {
        this.name = name;
        return this;
    }

    public WorkoutType setId(@ProgressType int id) {
        this.id = id;
        return this;
    }

    public static ArrayList<WorkoutType> getTypeForProgress(Context context) {
        ArrayList<WorkoutType> types = new ArrayList<>();
        for (int nameId : TYPE) {
            types.add(new WorkoutType().setName(context.getString(nameId))
                .setId(nameId == R.string.progress_type_weight_pic
                        ?ProgressType.TYPE_WEIGHT_PICTURE: ProgressType.TYPE_STRENGTH));
        }
        return types;
    }

    public String getTypeName() {
        return name;
    }

    @ProgressType
    public int getId() {
        return id;
    }
}

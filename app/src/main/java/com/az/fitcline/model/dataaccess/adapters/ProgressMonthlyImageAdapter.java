package com.az.fitcline.model.dataaccess.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.entities.ProgressMonthlyWeight;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Wajahat ${FILE} ${FILE} on 25/02/2018.
 */

public class ProgressMonthlyImageAdapter extends RecyclerView.Adapter <ProgressMonthlyImageAdapter.ViewHolder> {

    private ArrayList <Integer> mImage;
    private Context mContext;

    public ProgressMonthlyImageAdapter(Context mContext, ArrayList <Integer> mImage) {
        this.mImage = mImage;
        this.mContext = mContext;
    }

    @Override
    public ProgressMonthlyImageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return new ProgressMonthlyImageAdapter.ViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.row_monthly_weight_image;
    }

    public int getItemCount() {
        return mImage == null ? 0 : mImage.size();
    }


    @Override
    public void onBindViewHolder(ProgressMonthlyImageAdapter.ViewHolder holder, int position) {
        holder.bindData(mImage.get(position));

    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_monthly_weight_image)
        ImageView mIvMonthImage;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindData(Integer workoutImage) {
            mIvMonthImage.setImageResource(workoutImage);
        }
    }
}

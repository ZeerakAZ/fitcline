package com.az.fitcline.model.dataaccess.validations;

import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.widget.TextView;

/**
 * Created by Administrator on 5/12/2017.
 */

public class EmptyValidation implements Validation {
    private String errorMessage;
    private TextView textView;
    private Integer min;
    private Integer max;
    private TextInputLayout tIl;
    private String heading;

    public EmptyValidation(String message, TextView textView, String heading) {
        this.errorMessage = message;
        this.textView = textView;
        this.heading=heading;
    }

    public TextView getTextView(){
        return textView;
    }

    public EmptyValidation(String message, String heading, TextView textView, TextInputLayout tIl) {
        this.errorMessage = message;
        this.textView = textView;
        this.tIl = tIl;
        this.heading=heading;
    }

    public EmptyValidation(String message, String heading, TextView textView,
                           Integer min, Integer max) {
        this.errorMessage = message;
        this.textView = textView;
        this.min = min;
        this.max = max;
        this.heading=heading;
    }

    @Override
    public boolean passes() {
        if (min != null && max != null) {
            if (textView.getText().length() > max || textView.getText().length() < min) {
                textView.setError(errorMessage);
                textView.requestFocus();
                return false;
            } else {
                textView.setError(null);
                return true;
            }
        } else if (TextUtils.isEmpty(textView.getText().toString().trim())) {
            if (tIl != null) {
                tIl.setError(errorMessage);
                tIl.setErrorEnabled(true);
            } else {
                textView.setError(errorMessage);
                textView.requestFocus();
            }
            return false;
        } else if (tIl != null) {
            tIl.setErrorEnabled(false);
            tIl.setError("");
        }
        return true;
    }

    @Override
    public String getMessage() {
        if (min != null) {
            return errorMessage;
        }
        if (tIl != null)
            return errorMessage;
        else
            return errorMessage;
    }

    @Override
    public String getHeading() {
        return heading;
    }
}

package com.az.fitcline.model.dataaccess.entities;

import android.content.Context;

import com.az.fitcline.R;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Wajahat on 25/02/2018.
 */

public class ProgressMonthlyWeight {

    private static int[] MONTHLY_WEIGHT = new int[] {
            R.string.progress_month_july,R.string.progress_month_june,R.string.progress_month_august,
            R.string.progress_month_sep,R.string.progress_month_oct
    };

    private static int[] MONTHLY_IMAGE = new int[] {
            R.drawable.progress_monthly_weight4,R.drawable.progress_monthly_weight5,R.drawable.progress_monthly_weight4,
            R.drawable.progress_monthly_weight4,R.drawable.progress_monthly_weight5,R.drawable.progress_monthly_weight4,
            R.drawable.progress_monthly_weight4,R.drawable.progress_monthly_weight5,R.drawable.progress_monthly_weight4
    };

    private String month;
    private String weight;
    private ArrayList<Integer> monthImage;


    public String getMonth() {
        return month;
    }

    public String getWeight() {
        return weight;
    }

    public ArrayList <Integer> getMonthImage() {
        return monthImage;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public void setMonthImage(ArrayList <Integer> monthImage) {
        this.monthImage = monthImage;
    }


    ///dummy method
    public static ArrayList<ProgressMonthlyWeight> getMonth(Context context){
        ArrayList<ProgressMonthlyWeight> months = new ArrayList <>();
        for (int month : MONTHLY_WEIGHT) {
            ProgressMonthlyWeight progress = new ProgressMonthlyWeight();
            progress.setMonth(context.getString(month));
            progress.setWeight(String.format(Locale.ENGLISH,"%d lbs",((int )(Math.random()*10))));
            ArrayList<Integer> images = new ArrayList <>();
            for (int image : MONTHLY_IMAGE) {
                images.add(image);
            }
            progress.setMonthImage(images);
            months.add(progress);
        }
        return months;
    }

}

package com.az.fitcline.model.dataaccess.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.az.fitcline.model.dataaccess.entities.TabModel;
import com.az.fitcline.views.fragments.BaseFragment;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Zeera on 1/6/2018 bt ${File}
 */

public class TabAdapter extends FragmentStatePagerAdapter {

    ArrayList<TabModel> mTabs = new ArrayList<>();

    public TabAdapter(FragmentManager fm) {
        super(fm);
    }

    public void addTab(TabModel...models) {
        mTabs.addAll(Arrays.asList(models));
    }

    @Override
    public Fragment getItem(int position) {
        return mTabs.get(position).getFragment();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTabs.get(position).getmTabName();
    }

    @Override
    public int getCount() {
        return mTabs.size();
    }




}

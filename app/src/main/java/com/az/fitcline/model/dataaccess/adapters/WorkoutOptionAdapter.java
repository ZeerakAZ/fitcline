package com.az.fitcline.model.dataaccess.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.entities.WorkOutOptionModel;
import com.az.fitcline.model.dataaccess.validations.TextValidationWatcher;
import com.az.fitcline.views.customviews.EditTextCustom;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Optional;

/**
 * Created by Zeera on 12/30/2017 bt ${File}
 */

public class WorkoutOptionAdapter extends RecyclerView.Adapter<WorkoutOptionAdapter.ViewHolder> {

    private List<WorkOutOptionModel> mOptions;
    private Context mContext;
    private HashMap<Integer, Integer> mSelectedOption;
    private String mLengthValue;

    public WorkoutOptionAdapter(Context mContext, List<WorkOutOptionModel> groups) {
        this.mOptions = groups;
        this.mContext = mContext;
        mSelectedOption = new HashMap<>();
    }


    @Override
    public WorkoutOptionAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(viewType, parent, false);
        return new WorkoutOptionAdapter.ViewHolder(view, viewType);
    }

    @Override
    public int getItemViewType(int position) {
        switch (mOptions.get(position).getType()) {
            case WorkOutOptionModel.TYPE_DISPLAY:
                return R.layout.row_workout_option_display;
            case WorkOutOptionModel.TYPE_INPUT:
                return R.layout.row_workout_option_input;
        }
        return 0;
    }

    @Override
    public void onBindViewHolder(WorkoutOptionAdapter.ViewHolder holder, int position) {
        holder.bindData(mOptions.get(position), position == 0 ? null : mOptions.get(position - 1));
    }

    @Override
    public int getItemCount() {
        return mOptions == null ? 0 : mOptions.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_header)
        TextView mTvHeader;
        @BindView(R.id.divider)
        @Nullable
        View mDivider;
        @BindView(R.id.tv_option)
        @Nullable
        TextView mTvOption;
        @BindView(R.id.et_option)
        @Nullable
        EditTextCustom mEtOption;
        @Nullable
        @BindView(R.id.iv_arrow)
        ImageView mIvArrow;


        ViewHolder(View itemView, int itemViewType) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        @Optional
        @OnClick(R.id.tv_option)
        void selectOption() {
            WorkOutOptionModel workOutOptionModel = mOptions.get(getAdapterPosition());
            if (mSelectedOption.containsKey(workOutOptionModel.getHeaderResId()) &&
                    mSelectedOption.get(workOutOptionModel.getHeaderResId()) ==
                            workOutOptionModel.getDisplayValue()) {
                mSelectedOption.remove(workOutOptionModel.getHeaderResId());
            } else {
                mSelectedOption.put(workOutOptionModel.getHeaderResId(), workOutOptionModel.getDisplayValue());
            }
            notifyDataSetChanged();
        }

        void bindData(WorkOutOptionModel optionModel, @Nullable WorkOutOptionModel optionModelPrevious) {

            mTvHeader.setText(optionModel.getHeaderResId());
            boolean isHeaderShown = setHeaderVisibility(optionModel, optionModelPrevious);

            switch (getItemViewType()) {
                case R.layout.row_workout_option_display:
                    mTvOption.setText(optionModel.getDisplayValue());
                    mDivider.setVisibility(isHeaderShown ? View.GONE : View.VISIBLE);
                    mIvArrow.setVisibility(isSelected(optionModel)?View.VISIBLE:View.INVISIBLE);
                    mTvOption.setTextColor(ContextCompat.getColor(mContext,isSelected(optionModel)?
                            R.color.android_green:R.color.dimgray));
                    break;
                case R.layout.row_workout_option_input:
                    mEtOption.setHint(optionModel.getDisplayValue());
                    mEtOption.addTextChangeListener(updateText -> {
                        mLengthValue = updateText;
                    });
                    break;
            }
        }

        private boolean isSelected(WorkOutOptionModel optionModel){
            return mSelectedOption.containsKey(optionModel.getHeaderResId())
                    &&mSelectedOption.get(optionModel.getHeaderResId())==optionModel.getDisplayValue();

        }

        private boolean setHeaderVisibility(WorkOutOptionModel optionModel,
                                            @Nullable WorkOutOptionModel optionModelPrevious) {
            if (optionModelPrevious == null || optionModelPrevious.getHeaderResId() != optionModel.getHeaderResId()) {
                mTvHeader.setVisibility(View.VISIBLE);
                return true;
            } else {
                mTvHeader.setVisibility(View.GONE);
                return false;
            }
        }


    }

}

package com.az.fitcline.model.dataaccess.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.entities.CommentModel;
import com.az.fitcline.model.dataaccess.entities.PostModel;
import com.az.fitcline.model.utilities.FragmentUtility;
import com.az.fitcline.views.fragments.feedmodule.PostFragment;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zeera on 12/10/2017 bt ${File}
 */

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.ViewHolder> {

    private ArrayList<PostModel> mPosts;
    private Context mContext;
    private IPostClickListener mListerner;

    public PostAdapter( Context mContext,ArrayList<PostModel> mPosts, IPostClickListener mListerner) {
        this.mPosts = mPosts;
        this.mContext = mContext;
        this.mListerner = mListerner;
    }

    public PostAdapter(ArrayList<PostModel> mPosts, Context mContext) {
        this.mPosts = mPosts;
        this.mContext = mContext;
    }

    @Override
    public PostAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(viewType, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.row_post_text;
    }

    @Override
    public void onBindViewHolder(PostAdapter.ViewHolder holder, int position) {
        holder.bindData(mPosts.get(position));
    }

    @Override
    public int getItemCount() {
        return mPosts == null ? 0 : mPosts.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_comment_count)
        TextView mTvCommentsCount;
        @BindView(R.id.tv_like_count)
        TextView mTvLikesCount;
        @BindView(R.id.tv_post)
        TextView mTvPost;
        @BindView(R.id.iv_post_image)
        ImageView mIvPostImage;
        @BindView(R.id.rv_comments)
        RecyclerView mRvComments;
        @BindView(R.id.tv_username)
        TextView mtvPosterName;
        @BindView(R.id.tv_post_time)
        TextView mTvPostTime;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mRvComments.setLayoutManager(new LinearLayoutManager(mContext));
        }

        void bindData(PostModel data) {
            mTvCommentsCount.setText(String.format(Locale.ENGLISH,
                    "%d", data.getCommentsCount()));
            mTvLikesCount.setText(String.format(Locale.ENGLISH,
                    "%d", data.getLikesCount()));
            mTvPost.setText(data.getPost());
            mtvPosterName.setText(data.getUserModel().getUserName());
            mTvPostTime.setText(data.getPostTime());
            setComments(data.getComments());
            if (data.getImagePath() != null) {
                mIvPostImage.setVisibility(View.VISIBLE);
                /*Picasso.with(mContext)
                        .load(data.getImagePath())
                        .into(mIvPostImage);*/
            } else {
                mIvPostImage.setVisibility(View.GONE);
            }
            mTvPost.setOnClickListener(v -> {
                if (mListerner != null) {
                    mListerner.onPostClick(data);
                }
            });

            mIvPostImage.setOnClickListener(v ->{
                if (mListerner != null) {
                    mListerner.onPostClick(data);
                }
            });


        }

        private void setComments(@Nullable ArrayList<CommentModel> comments) {
            List<CommentModel> commentToShow = null;
            if (comments != null) {
                if (comments.size() > 2)
                    commentToShow = comments.subList(comments.size() - 2, comments.size() - 1);
                else
                    commentToShow = comments;
                mRvComments.setAdapter(new CommentsAdapter(mContext, commentToShow,
                        CommentsAdapter.TYPE_COMMENT_ONLY));
                mRvComments.setVisibility(View.VISIBLE);
            } else {
                mRvComments.setVisibility(View.GONE);
            }
        }



    }

    @FunctionalInterface
    public interface IPostClickListener{
        void onPostClick(PostModel postModel);
    }
}

package com.az.fitcline.model.dataaccess.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.entities.ImageModel;

import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by Zeera on 12/13/2017 bt ${File}
 */

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ViewHolder> {
    private List<ImageModel> mImages;
    private Context mContext;

    public ImagesAdapter(List<ImageModel> mImages, Context mContext) {
        this.mImages = mImages;
        this.mContext = mContext;
    }


    @Override
    public ImagesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(viewType, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.row_image;
    }

    @Override
    public void onBindViewHolder(ImagesAdapter.ViewHolder holder, int position) {
        holder.bindData(mImages.get(position));
    }

    @Override
    public int getItemCount() {
        return mImages == null ? 0 : mImages.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {


        ViewHolder(View itemView) {
            super(itemView);
        }
        void bindData(ImageModel imageModel){
            // TODO: 12/13/2017 future things goes here bra!!!
        }
    }
}

package com.az.fitcline.model.dataaccess.di.modules;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.az.fitcline.model.dataaccess.database.MyDatabase;
import com.az.fitcline.model.dataaccess.database.dao.UserDao;
import com.az.fitcline.model.dataaccess.network.IApiInterface;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by zeerak on 6/2/2018 bt ${File}
 */
@Module()
public class AppModule {
    // --- DATABASE INJECTION ---

    @Provides
    @Singleton
    MyDatabase provideDatabase(Application application) {
        return Room.databaseBuilder(application,
                MyDatabase.class, "MyDatabase.db")
                .build();
    }

    @Provides
    @Singleton
    UserDao provideUserDao(MyDatabase database) {
        return database.userDao();
    }

    // --- REPOSITORY INJECTION ---


    // --- NETWORK INJECTION ---

    private static String BASE_URL = "https://api.github.com/";

    @Provides
    Gson provideGson() { return new GsonBuilder().create(); }

    @Provides
    Retrofit provideRetrofit(Gson gson) {
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(BASE_URL)
                .build();
        return retrofit;
    }

    @Provides
    @Singleton
    IApiInterface provideApiWebservice(Retrofit restAdapter) {
        return restAdapter.create(IApiInterface.class);
    }
}

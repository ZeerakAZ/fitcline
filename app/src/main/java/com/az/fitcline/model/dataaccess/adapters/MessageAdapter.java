package com.az.fitcline.model.dataaccess.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.entities.MessageModel;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zeera on 12/17/2017 bt ${File}
 */

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {
    private List<MessageModel> mMessages;
    private Context mContext;

    public MessageAdapter(Context mContext, List<MessageModel> mMessages) {
        this.mMessages = mMessages;
        this.mContext = mContext;
    }

    public void addMessage(MessageModel...messageModel){
        if(mMessages!=null){
            int startPoint = mMessages.size();
            mMessages.addAll(Arrays.asList(messageModel));
            notifyItemRangeInserted(startPoint,messageModel.length);
        }
    }


    @Override
    public MessageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(viewType, parent, false);
        return new MessageAdapter.ViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return mMessages.get(position).isMe() ? R.layout.row_message_sender : R.layout.row_message_receiver;
    }

    @Override
    public void onBindViewHolder(MessageAdapter.ViewHolder holder, int position) {
        holder.bindData(mMessages.get(position));
    }

    @Override
    public int getItemCount() {
        return mMessages == null ? 0 : mMessages.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_message)
        TextView mTvMessage;
        @BindView(R.id.tv_message_time)
        TextView mTvMessageTime;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindData(MessageModel messageModel) {
            mTvMessage.setText(messageModel.getMessageText());
            mTvMessageTime.setText(String.format(Locale.ENGLISH, "%d mins",
                    messageModel.getMessageTime()));
        }
    }
}

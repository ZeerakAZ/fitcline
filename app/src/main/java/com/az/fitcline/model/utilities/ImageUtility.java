package com.az.fitcline.model.utilities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

import com.az.fitcline.model.dataaccess.entities.BottomSheetOptionModel;
import com.az.fitcline.model.dataaccess.interfaces.BottomOptionActions;
import com.az.fitcline.views.dialogs.GenericBottomSheetDialog;
import com.az.fitcline.views.fragments.BaseFragment;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Zeera on 4/5/2017.
 * <p>
 * Created by ZeeraMu on 8/30/2017 lexicon-android
 * <p>
 * Created by ZeeraMu on 8/30/2017 lexicon-android
 */
/**
 * Created by ZeeraMu on 8/30/2017 lexicon-android
 */

/**
 * Created by ZeeraMu on 4/5/2017 iwant-android
 */

public class ImageUtility implements GenericBottomSheetDialog.IBottomOptionSelectionListener{
    public static final String IMAGE_PATH = "photo.png";
    public static final int REQUEST_PERMISSIONS = 0xf01;

    private int selectFile = 5679;
    private int requestCamera = 5678;
    private Activity activity;
    private BaseFragment fragment;
    private ImageView imageView;
    private String imagePath;
    private String imageBase64;
    private GenericBottomSheetDialog bottomSheetDialog;

    public ImageUtility(Activity activity, ImageView imageView, int codeCamera, int codeFile) {
        this.activity = activity;
        this.imageView = imageView;
        this.requestCamera = codeCamera;
        this.selectFile = codeFile;


        this.bottomSheetDialog = new GenericBottomSheetDialog(activity,
                new ArrayList<BottomSheetOptionModel>(){{
                   add(BottomOptionActions.ACTION_OPEN_CAMERA);
                   add(BottomOptionActions.ACTION_OPEN_GALLERY);
                }},this);
    }

    public ImageUtility(BaseFragment fragment, ImageView imageView, int codeCamera, int codeFile) {
        this.activity = fragment.getActivity();
        this.fragment = fragment;
        this.imageView = imageView;
        this.requestCamera = codeCamera;
        this.selectFile = codeFile;

        this.bottomSheetDialog = new GenericBottomSheetDialog(activity,
                new ArrayList<BottomSheetOptionModel>(){{
                    add(BottomOptionActions.ACTION_OPEN_CAMERA);
                    add(BottomOptionActions.ACTION_OPEN_GALLERY);
                }},this);
    }

    private void openCamera() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imagePath);
        if (fragment != null)
            fragment.startActivityForResult(intent, requestCamera);
        else
            activity.startActivityForResult(intent, requestCamera);
    }

    private void openGallery() {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        if (fragment != null)
            fragment.startActivityForResult(
                    Intent.createChooser(intent, "Select Image"),
                    selectFile);
        else
            activity.startActivityForResult(
                    Intent.createChooser(intent, "Select Image"),
                    selectFile);
    }

    public void setImageFromCameraResult(Intent data) {
        Bundle extras = data.getExtras();
        Bitmap imageBitmap = (Bitmap) extras.get("data");
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        setBase64EncodedImage(stream);
        imageView.setImageBitmap(imageBitmap);
        Uri tempUri = getImageUri(imageBitmap);
        File finalFile = new File(getRealPathFromURI(tempUri));
        imagePath = finalFile.getPath();
    }

    private Uri getImageUri(Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(activity.getContentResolver(),
                inImage, "img_", null);
        return Uri.parse(path);
    }

    private String getRealPathFromURI(Uri uri) {
        Cursor cursor = activity.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public void convertAndSetImageToUI(Intent data) {
        if (fragment != null)
            activity = fragment.getActivity();
        new AsynImageLoadImage().executeOnExecutor
                (AsyncTask.THREAD_POOL_EXECUTOR, data);
    }

    private String saveImageToInternalStorage(Bitmap image) {
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            Log.d("Lexicon", "Error creating media file, check storage permissions: ");// e.getMessage());
            return null;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 70, fos);

            fos.close();
            return pictureFile.toString();
        } catch (FileNotFoundException e) {
            Log.d("Lexicon", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("Lexicon", "Error accessing file: " + e.getMessage());
        }
        return null;
    }

    private File getOutputMediaFile() {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + activity.getPackageName()
                + "/Files");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        File mediaFile;
        String mImageName = "MI_" + timeStamp + ".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }


    private Bitmap rotateImage(Bitmap bm, int i) {
        int width = bm.getWidth();
        int height = bm.getHeight();

        Matrix matrix;
        matrix = new Matrix();
        matrix.postRotate(i);

        return Bitmap.createBitmap(bm, 0, 0,
                width, height, matrix, true);
    }

    private void setBase64EncodedImage(ByteArrayOutputStream stream) {
        imageBase64 = Base64.encodeToString(stream.toByteArray(), Base64.DEFAULT);
    }

    public boolean checkCameraAndExternalStoragePermissions() {
        //when permissions are already granted
        if (fragment != null) {
            if (PermissionsUtitlty.isCameraPermissionAllowed(fragment.getActivity())
                    && PermissionsUtitlty.isExternalStoragePermissionAllowed(fragment.getActivity())) {
                bottomSheetDialog.show();
                return true;

            }
            PermissionsUtitlty.showPermissionsDialogForFragment(fragment, REQUEST_PERMISSIONS, Manifest.permission.CAMERA
                    , Manifest.permission.WRITE_EXTERNAL_STORAGE);

        } else {
            if (PermissionsUtitlty.isCameraPermissionAllowed(activity)
                    && PermissionsUtitlty.isExternalStoragePermissionAllowed(activity)) {
                bottomSheetDialog.show();
                return true;
            }
            PermissionsUtitlty.showPermissionsDialogForActivity(activity, REQUEST_PERMISSIONS, Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE);

        }
        return false;
    }

    public String getImagePath() {
        return imagePath;
    }

    public String getImageBase64() {
        return imageBase64;
    }

    @SuppressLint("SwitchIntDef")
    @Override
    public void performActionPlease(int actionId) {
        switch (actionId) {
            case BottomOptionActions.OPEN_CAMERA:
                openCamera();
                break;
            case BottomOptionActions.OPEN_GALLERY:
                openGallery();
                break;
        }
    }

    public class AsynImageLoadImage extends AsyncTask<Intent, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(Intent... params) {
            Uri selectedImageUri = params[0].getData();
            Bitmap bm = null;
            if (selectedImageUri.toString().startsWith("content://com.google.android.apps.photos.content")
                    || selectedImageUri.toString().startsWith("file://")) {
                try {
                    InputStream is = activity.getContentResolver().openInputStream(selectedImageUri);
                    if (is != null) {
                        bm = resizeBitmapByScale(BitmapFactory.decodeStream(is), 0.125f, true);
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        setBase64EncodedImage(stream);
                        imagePath = saveImageToInternalStorage(bm);

                        //You can use this bitmap according to your purpose or Set bitmap to imageview
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
            String selectedImagePath = null;
            if (bm == null) {
                String[] projection = {MediaStore.MediaColumns.DATA};
                Cursor cursor = activity.getContentResolver().query(selectedImageUri, projection, null, null,
                        null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                selectedImagePath = cursor.getString(column_index);
                bm = decodeFile(new File(selectedImagePath));
            }
            if (selectedImagePath != null) {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();

                try {
                    ExifInterface ei = new ExifInterface(selectedImagePath);
                    int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                    switch (orientation) {
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            bm = rotateImage(bm, 90);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            bm = rotateImage(bm, 180);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_270:
                            bm = rotateImage(bm, 270);
                            break;
                        // etc.
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
                imagePath = saveImageToInternalStorage(bm);
                setBase64EncodedImage(stream);
            }
            return bm;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            if (bitmap != null)
                imageView.setImageBitmap(bitmap);

        }
    }

    private Bitmap decodeFile(File f) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            // The new size we want to scale to
            final int REQUIRED_SIZE = 70;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
        }
        return null;
    }

    private Bitmap resizeBitmapByScale(
            Bitmap bitmap, float scale, boolean recycle) {
        int width = Math.round(bitmap.getWidth() * scale);
        int height = Math.round(bitmap.getHeight() * scale);
        if (width == bitmap.getWidth()
                && height == bitmap.getHeight()) return bitmap;
        Bitmap target = Bitmap.createBitmap(width, height, getConfig(bitmap));
        Canvas canvas = new Canvas(target);
        canvas.scale(scale, scale);
        Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG | Paint.DITHER_FLAG);
        canvas.drawBitmap(bitmap, 0, 0, paint);
        if (recycle) bitmap.recycle();
        return target;
    }

    private Bitmap.Config getConfig(Bitmap bitmap) {
        Bitmap.Config config = bitmap.getConfig();
        if (config == null) {
            config = Bitmap.Config.ARGB_8888;
        }
        return config;
    }

    public void showSelectionDialog(){
        bottomSheetDialog.show();
    }
}




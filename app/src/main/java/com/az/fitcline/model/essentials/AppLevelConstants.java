package com.az.fitcline.model.essentials;

/**
 * Created by Zeera on 11/19/2017 bt ${File}
 */


/**
 * this class is used to define constant which is required on global levels only use it for true
 * global variables.If related to a module must be define in corresponding classes
 */
public class AppLevelConstants {
}

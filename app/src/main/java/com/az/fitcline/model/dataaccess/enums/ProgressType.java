package com.az.fitcline.model.dataaccess.enums;

import android.support.annotation.IntDef;

/**
 * Created by Zeera on 3/3/2018 bt ${File}
 */
@IntDef({ProgressType.TYPE_STRENGTH, ProgressType.TYPE_WEIGHT_PICTURE})
public @interface ProgressType {
    int TYPE_WEIGHT_PICTURE = 0xd01;
    int TYPE_STRENGTH = 0xd02;
}

package com.az.fitcline.model.dataaccess.entities;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Zeera on 12/18/2017 bt ${File}
 */

public class GroupModel implements Serializable {

    private static final String[] tags = new String[]{
            "#awwesomeBuilders","#theRocks","#slimyfreaks"
            ,"#bigGuns","#loser","#popShppers"
    };
    private String groupTag;
    private boolean isAdmin;
    private boolean isPrivate;
    private ArrayList<GroupMember> mMembers;




    public String getGroupTag() {
        return groupTag;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public ArrayList<GroupMember> getmMembers() {
        return mMembers;
    }

    public static GroupModel getRandomGroup(){
        int random = ((int) (Math.random() * (tags.length - 1)));
        GroupModel model = new GroupModel();
        model.groupTag = tags[random];
        model.isAdmin = random%2==0;
        model.isPrivate = random%4==0;
        model.mMembers = new ArrayList<>();
        for (int i = 0; i < random; i++) {
            model.mMembers.add(GroupMember.getRandomGroupMember());
        }
        return model;
    }

}

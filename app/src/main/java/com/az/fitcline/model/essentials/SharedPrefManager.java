package com.az.fitcline.model.essentials;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Collection;

/**
 * Created by Administrator on 11/11/2016 bt ${File}
 */

public class SharedPrefManager {

    private SharedPreferences sharedpreferences;
    private static SharedPrefManager mInstance;
    private boolean isInBackgroud = true;

    public static SharedPrefManager getInstance(Context context) {
        if(mInstance==null)
            mInstance = new SharedPrefManager(context.getApplicationContext());
        return mInstance;
    }

    private SharedPrefManager(Context context) {
        sharedpreferences = context.getSharedPreferences(context.getPackageName(),
                Context.MODE_PRIVATE);
    }

    public void clearPreferences() {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.clear();
        if (isInBackgroud) {
            editor.apply();
        }else
            editor.commit();
    }

    public void setInBackgroud(boolean inBackgroud) {
        isInBackgroud = inBackgroud;
    }

    public void setBooleanForKey(Boolean val, String key) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean(key, val);
        if (isInBackgroud) {
            editor.apply();
        }else
            editor.commit();
    }

    public void setStringForKey(String val, String key) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(key, val);
        if (isInBackgroud) {
            editor.apply();
        }else
            editor.commit();
    }

    public void setIntegerForKey(int val, String key) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt(key, val);
        if (isInBackgroud) {
            editor.apply();
        }else
            editor.commit();
    }

    public void setLongForKey(long val, String key) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putLong(key, val);
        if (isInBackgroud) {
            editor.apply();
        }else
            editor.commit();
    }



    public Boolean getBooleanByKey(String key) {
        if (sharedpreferences.contains(key)) {
            return sharedpreferences.getBoolean(key, false);
        }
        return false;
    }

    public String getStringByKey(String key){
        if (sharedpreferences.contains(key)) {
            return sharedpreferences.getString(key, "");
        }
        return null;
    }

    public int getIntegerByKey(String key){
        if (sharedpreferences.contains(key)) {
            return sharedpreferences.getInt(key, 0);
        }
        return 0;
    }

    public long getLongByKey(String key){
        if (sharedpreferences.contains(key)) {
            return sharedpreferences.getLong(key, 0);
        }
        return 0;
    }

    public void saveCollectionTOSharedPref(Object collection, String key){
        String value = new Gson().toJson(collection);
        setStringForKey(value,key);
    }


    public Collection getCollectionFromSharedPref(String key){
        String value = getStringByKey(key);
        GsonBuilder gsonb = new GsonBuilder();
        Gson gson = gsonb.create();
        return gson.fromJson(value, Collection.class);

    }

    public void saveObjectInSharedPref(String key, Object obj) {
        Gson gson = new Gson();
        String json = gson.toJson(obj);
        setStringForKey(json, key);
    }

    public <T> T getObjectFromSharedPref(String key, Class<T> toCast) {
        Gson gson = new Gson();
        String json = getStringByKey(key);
        if (json!=null&&!json.equals("")){
            return  gson.fromJson(json,toCast);
        }else{
            return null;
        }
    }

}

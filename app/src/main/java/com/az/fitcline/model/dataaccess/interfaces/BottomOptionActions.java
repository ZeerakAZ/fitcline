package com.az.fitcline.model.dataaccess.interfaces;

import android.support.annotation.IntDef;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.entities.BottomSheetOptionModel;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Zeera on 12/16/2017 bt ${File}
 */

@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.FIELD,
        ElementType.PARAMETER, ElementType.METHOD})
@IntDef({BottomOptionActions.DELETE_COMMENT,
        BottomOptionActions.REPORT_SPAM,
        BottomOptionActions.EDIT_COMMENT,
        BottomOptionActions.BLOCK_USER,
        BottomOptionActions.OPEN_GALLERY,
        BottomOptionActions.OPEN_CAMERA
})
public @interface BottomOptionActions {
    int REPORT_SPAM = 0xf01;
    int EDIT_COMMENT = 0xf02;
    int DELETE_COMMENT = 0xf03;
    int BLOCK_USER = 0xf04;
    int OPEN_CAMERA = 0xf05;
    int OPEN_GALLERY = 0xf06;

    BottomSheetOptionModel ACTION_REPORT_SPAM = new BottomSheetOptionModel(REPORT_SPAM,
            R.string.action_report_spam);
    BottomSheetOptionModel ACTION_EDIT_COMMENT = new BottomSheetOptionModel(EDIT_COMMENT,
            R.string.action_edit_comment);
    BottomSheetOptionModel ACTION_DELETE_COMMENT = new BottomSheetOptionModel(DELETE_COMMENT,
            R.string.action_delete_comment);
    BottomSheetOptionModel ACTION_BLOCK_USER = new BottomSheetOptionModel(BLOCK_USER,
            R.string.action_block_user);
    BottomSheetOptionModel ACTION_OPEN_CAMERA = new BottomSheetOptionModel(OPEN_CAMERA,
            R.string.action_open_camera, R.drawable.ic_camera);
    BottomSheetOptionModel ACTION_OPEN_GALLERY = new BottomSheetOptionModel(OPEN_GALLERY,
            R.string.action_open_gallery, R.drawable.ic_gallery);

}



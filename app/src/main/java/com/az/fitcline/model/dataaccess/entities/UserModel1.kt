package com.az.fitcline.model.dataaccess.entities

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class UserModel1(
    @SerializedName("FirstName") val firstName: String,
    @SerializedName("LastName") val lastName: String,
    @SerializedName("Email") val email: String,
    @SerializedName("Username") val username: String,
    @SerializedName("Password") val password: String,
    @SerializedName("Token") val token: String
):Serializable
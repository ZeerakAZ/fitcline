package com.az.fitcline.model.dataaccess.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.entities.ProgressOverview;
import com.az.fitcline.model.dataaccess.entities.WorkoutImage;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Wajahat on 25/02/2018.
 */

public class ProgressOverviewImageAdapter extends RecyclerView.Adapter<ProgressOverviewImageAdapter.ViewHolder>{

    private ArrayList<ProgressOverview> mImage;
    private Context context;
    private int mSelectedItem;

    public ProgressOverviewImageAdapter (Context context, ArrayList <ProgressOverview> mImage){
        this.mImage=mImage;
        this.context=context;
    }



    public ProgressOverviewImageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view= LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);

        return new ProgressOverviewImageAdapter.ViewHolder(view);
    }


    @Override

    public int getItemViewType(int position) {
        return R.layout.row_progress_overview;
    }

    public int getItemCount() {
        return mImage == null ? 0 : mImage.size();
    }


    @Override
    public void onBindViewHolder(ProgressOverviewImageAdapter.ViewHolder holder, int position) {
        holder.bindData(mImage.get(position));

    }
    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.progress_overview_monthly_picture)
        ImageView mMonthlyPicture;


        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindData(ProgressOverview imageMonth){

            mMonthlyPicture.setImageResource(imageMonth.getImageMonth());
        }

    }

}

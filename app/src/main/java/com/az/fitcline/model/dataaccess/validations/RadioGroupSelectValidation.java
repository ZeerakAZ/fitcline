package com.az.fitcline.model.dataaccess.validations;

import android.widget.RadioGroup;

/**
 * Created by zeerak on 6/9/2018 bt ${File}
 */
public class RadioGroupSelectValidation implements Validation {
    private RadioGroup mRg;
    private String mMessage;

    public RadioGroupSelectValidation(String mMessage, RadioGroup mRg) {
        this.mRg = mRg;
        this.mMessage = mMessage;
    }

    @Override
    public boolean passes() {
        boolean b = mRg.getCheckedRadioButtonId() == RadioGroup.NO_ID;
        if(b)
            mRg.requestFocus();
        return !b;
    }

    @Override
    public String getMessage() {
        return mMessage;
    }

    @Override
    public String getHeading() {
        return null;
    }
}

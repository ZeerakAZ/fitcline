package com.az.fitcline.model.dataaccess.validations;

import android.widget.TextView;

/**
 * Created by ZeeraMu on 5/19/2017 android
 */

public class TextCompareValidation implements Validation{
    private String errorMessage;
    private TextView textView;
    private String textToCompare;
    boolean shouldBeEqual;
    private String heading;

    public TextCompareValidation(String message, TextView textView, String textToCompare, String heading){
        this.errorMessage = message;
        this.textView = textView;
        this.textToCompare = textToCompare;
        this.heading=heading;
    }

    public TextCompareValidation setShouldBeEqual(boolean shouldBeEqual) {
        this.shouldBeEqual = shouldBeEqual;
        return this;
    }

    @Override
    public boolean passes() {
        if(shouldBeEqual){
            if(textView.getText().toString().equals(textToCompare))
                return true;
            else{
               textView.setError(errorMessage);
                textView.requestFocus();
                return false;
            }
        }else{
            if(!textView.getText().toString().equals(textToCompare))
                return true;
            else{
                textView.setError(errorMessage);
                textView.requestFocus();
                return false;
            }
        }
    }

    @Override
    public String getMessage() {
        return errorMessage;
    }

    @Override
    public String getHeading() {
        return heading;
    }
}

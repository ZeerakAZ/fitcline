package com.az.fitcline.model.dataaccess.entities;

import android.support.annotation.IntDef;
import android.support.annotation.StringRes;

import com.az.fitcline.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;

/**
 * Created by Zeera on 12/30/2017 bt ${File}
 */

public class WorkOutOptionModel {
    public static final int TYPE_DISPLAY = 0xf01;
    public static final int TYPE_INPUT = 0xf02;

    public static final int HEADER_GOAL = R.string.ph_goal;
    public static final int HEADER_LENGTH = R.string.ph_length;
    public static final int HEADER_DIFFICULTY = R.string.ph_difficulty;
    public static final int HEADER_GENDER = R.string.ph_for_gender;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({TYPE_DISPLAY, TYPE_INPUT})
    public @interface OptionTypes {
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({HEADER_GOAL, HEADER_LENGTH, HEADER_DIFFICULTY, HEADER_GENDER})
    @interface OptionHeader {
    }

    @OptionTypes
    private int type;
    @OptionHeader
    private int headerResId;
    @StringRes
    private int displayValue;


    private WorkOutOptionModel(int type, int headerResId, int displayValue) {
        this.type = type;
        this.headerResId = headerResId;
        this.displayValue = displayValue;
    }

    @WorkOutOptionModel.OptionTypes
    public int getType() {
        return type;
    }

    public int getHeaderResId() {
        return headerResId;
    }

    public int getDisplayValue() {
        return displayValue;
    }

    public static ArrayList<WorkOutOptionModel> getOptions() {
        ArrayList<WorkOutOptionModel> options = new ArrayList<>();
        options.add(new WorkOutOptionModel(TYPE_DISPLAY, HEADER_GOAL, R.string.goal_build_muscle));
        options.add(new WorkOutOptionModel(TYPE_DISPLAY, HEADER_GOAL, R.string.goal_lose_fat));
        options.add(new WorkOutOptionModel(TYPE_INPUT, HEADER_LENGTH, R.string.ph_days));
        options.add(new WorkOutOptionModel(TYPE_DISPLAY, HEADER_DIFFICULTY, R.string.difficulty_beginner));
        options.add(new WorkOutOptionModel(TYPE_DISPLAY, HEADER_DIFFICULTY, R.string.difficulty_intermediate));
        options.add(new WorkOutOptionModel(TYPE_DISPLAY, HEADER_DIFFICULTY, R.string.difficulty_advance));
        options.add(new WorkOutOptionModel(TYPE_DISPLAY, HEADER_GENDER, R.string.ph_any));
        options.add(new WorkOutOptionModel(TYPE_DISPLAY, HEADER_GENDER, R.string.ph_male));
        options.add(new WorkOutOptionModel(TYPE_DISPLAY, HEADER_GENDER, R.string.ph_female));
        return options;
    }
}

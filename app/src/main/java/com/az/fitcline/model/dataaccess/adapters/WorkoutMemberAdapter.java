package com.az.fitcline.model.dataaccess.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.entities.WorkoutMemberModel;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zeera on 1/6/2018 bt ${File}
 */

public class WorkoutMemberAdapter extends RecyclerView.Adapter<WorkoutMemberAdapter.ViewHolder> {
    private final List<WorkoutMemberModel> mUsers;
    private final Context mContext;

    public WorkoutMemberAdapter(Context context, List<WorkoutMemberModel> users) {
        mContext = context;
        mUsers = users;
    }

    @Override
    public WorkoutMemberAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(viewType, parent, false);
        return new WorkoutMemberAdapter.ViewHolder(view);
    }


    @Override
    public int getItemViewType(int position) {
        return R.layout.row_workout_member;
    }

    @Override
    public void onBindViewHolder(final WorkoutMemberAdapter.ViewHolder holder, int position) {
        holder.bindData(mUsers.get(position));
    }

    @Override
    public int getItemCount() {
        return mUsers == null ? 0 : mUsers.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_profile_pic)
        ImageView mIvProfileImage;
        @BindView(R.id.tv_username)
        TextView mTvUserName;
        @BindView(R.id.tv_month)
        TextView mTvWieght;
        @BindView(R.id.iv_medal)
        ImageView ivMedal;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, itemView);
        }

        void bindData(WorkoutMemberModel memberModel) {
            mIvProfileImage.setImageResource(memberModel.getUserModel().getProfilePicId());
            mTvUserName.setText(getDisplayName(memberModel,getAdapterPosition()));
            ivMedal.setImageResource(getMedal(getAdapterPosition()));
        }

        SpannableString getDisplayName(WorkoutMemberModel model,int pos){
            String string = String.format(Locale.ENGLISH,"%d. %s %s",pos+1,model.getUserModel().getUserName(),model.getPositionChanged());
            SpannableString spannableString = new SpannableString(string);
            if(model.getPositionChanged().length()>0){
                spannableString.setSpan(new ForegroundColorSpan(ContextCompat.
                                getColor(mContext,model.getPositionChanged().contains("-")?R.color.red:
                                R.color.android_green))
                        , string.length()-model.getPositionChanged().length()
                        , string.length(),
                        Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            }
            return spannableString;
        }

        int getMedal(int pos){
            switch (pos){
                case 0:
                    return R.drawable.medal_gold;
                case 1:
                    return R.drawable.medal_silver;
                case 2:
                    return R.drawable.medal_bronze;
                default:
                    if(pos<10)
                        return R.drawable.star;
                    else
                        return 0;
            }
        }
    }
}

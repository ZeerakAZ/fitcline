package com.az.fitcline.model.dataaccess.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.entities.WorkOutPostModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zeera on 12/17/2017 bt ${File}
 */

public class WorkoutListAdapter extends RecyclerView.Adapter<WorkoutListAdapter.ViewHolder> {

    private ArrayList<WorkOutPostModel> mPosts;
    private Context mContext;

    public WorkoutListAdapter(ArrayList<WorkOutPostModel> mPosts, Context mContext) {
        this.mPosts = mPosts;
        this.mContext = mContext;
    }

    @Override
    public WorkoutListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(viewType, parent, false);
        return new WorkoutListAdapter.ViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.row_workout_post;
    }

    @Override
    public void onBindViewHolder(WorkoutListAdapter.ViewHolder holder, int position) {
        holder.bindData(mPosts.get(position));
    }

    @Override
    public int getItemCount() {
        return mPosts == null ? 0 : mPosts.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_comment_count)
        TextView mTvCommentsCount;
        @BindView(R.id.tv_like_count)
        TextView mTvLikesCount;
        @BindView(R.id.tv_post)
        TextView mTvPost;
        @BindView(R.id.tv_username)
        TextView mtvPosterName;
        @BindView(R.id.tv_post_time)
        TextView mTvPostTime;
        @BindView(R.id.tv_difficulty)
        TextView mTvDifficulty;
        @BindView(R.id.tv_gender)
        TextView mTvGender;
        @BindView(R.id.tv_days)
        TextView mTvDays;
        @BindView(R.id.tv_gaol)
        TextView mTvGoal;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindData(WorkOutPostModel data) {
            mTvCommentsCount.setText(String.valueOf(data.getCommentsCount()));
            mTvLikesCount.setText(String.valueOf(data.getLikesCount()));
            mTvPost.setText(data.getPost());
            mtvPosterName.setText(data.getUserModel().getUserName());
            mTvPostTime.setText(data.getPostTime());
            mTvGender.setText(mContext.getString(R.string.ph_gender_number, data.
                    getGender().getDisplayValue(mContext)));
            mTvDifficulty.setText(mContext.getString(R.string.ph_difficulty_number,
                    data.getDifficulty().getName(mContext)));
            mTvDays.setText(mContext.getString(R.string.ph_days_number,data.getWorkOutTime()));
            mTvGoal.setText(data.getGoal());
        }


    }
}

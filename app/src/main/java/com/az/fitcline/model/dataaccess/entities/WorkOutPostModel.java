package com.az.fitcline.model.dataaccess.entities;

import android.support.annotation.StringRes;

import com.az.fitcline.model.dataaccess.enums.Gender;
import com.az.fitcline.model.dataaccess.enums.WorkDifficulty;

/**
 * Created by Zeera on 12/17/2017 bt ${File}
 */

public class WorkOutPostModel extends PostModel {
    private static String[] plans = new String[]{
            "Full body short week plan", "Bikini physique ready", "Shred monster volume 1"
            , "Shred monster volume 2", "Shred monster volume 3", "Shred monster volume 5",
            "Shred monster volume 6"
    };
    private static String[] goals = new String[]{
            "Lose Fat", "Build Muscle", "Become fat"
            , "Become slim", "Legs", "Arms",
            "Big Guns"
    };

    private String workOutTime;
    private WorkDifficulty difficulty;
    private Gender gender;
    private String plan;
    private String goal;

    public WorkOutPostModel(PostModel model) {
        super(model);
    }

    public WorkOutPostModel(String workOutTime, WorkDifficulty difficulty,
                            Gender gender, String plan) {
        super();
        this.workOutTime = workOutTime;
        this.difficulty = difficulty;
        this.gender = gender;
        this.plan = plan;
    }

    public String getWorkOutTime() {
        return workOutTime;
    }

    public WorkDifficulty getDifficulty() {
        return difficulty;
    }

    public Gender getGender() {
        return gender;
    }

    public String getPlan() {
        return plan;
    }

    public String getGoal() {
        return goal;
    }

    public static WorkOutPostModel getWorkOutModel(UserModel model) {
        int rand = (int) (Math.random() * (plans.length-1));
        WorkOutPostModel workOutPostModel = new WorkOutPostModel(getRandomPosts());
        workOutPostModel.setUserModel(model);
        workOutPostModel.gender = rand == 0 ? Gender.ANY : rand % 2 == 0 ? Gender.MALE : Gender.FEMALE;
        workOutPostModel.difficulty = rand == 0 ? WorkDifficulty.ADVANCE : rand % 2 == 0 ?
                WorkDifficulty.INTERMEDIATE : WorkDifficulty.BEGINNER;
        workOutPostModel.goal = goals[rand];
        workOutPostModel.setPost(plans[rand]);
        workOutPostModel.workOutTime = rand+"";
        return workOutPostModel;
    }

}


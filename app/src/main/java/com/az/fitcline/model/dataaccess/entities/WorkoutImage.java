package com.az.fitcline.model.dataaccess.entities;

import android.content.Context;

import com.az.fitcline.R;

import java.util.ArrayList;

/**
 * Created by Wajahat on 23/02/2018.
 */

public class WorkoutImage {

    private static int[] WeightImage = new int[] {
            R.drawable.dummy_image,R.drawable.dummy_image,R.drawable.dummy_image,R.drawable.dummy_image,R.drawable.dummy_image,
            R.drawable.dummy_image,R.drawable.dummy_image,R.drawable.dummy_image
    };
    private int imageName;

    public void setImageName(int imageName){
        this.imageName=imageName;
    }

    public static ArrayList<WorkoutImage> getImageID(Context context){
        ArrayList<WorkoutImage> images = new ArrayList <>();
        for (int i : WeightImage) {
            WorkoutImage WeightImage = new WorkoutImage();
            WeightImage.setImageName(i);
            images.add(WeightImage);
        }
        return images;
    }


    public int getImageName(){
        return imageName;
    }

}

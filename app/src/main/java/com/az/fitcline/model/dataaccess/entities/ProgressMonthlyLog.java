package com.az.fitcline.model.dataaccess.entities;

import android.content.Context;

import com.az.fitcline.R;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Wajahat ${FILE} on 27/02/2018.
 */

public class ProgressMonthlyLog {


    private static int[] MONTH = new int[]{
            R.string.progresslog_month_aug, R.string.progresslog_month_dec, R.string.progresslog_month_nov, R.string.progresslog_month_sept,
    };

    private static int[] EXERCISE = new int[]{
            R.string.progress_exercise_squat, R.string.progress_exercise_deadlift, R.string.progress_exercise_bench
    };

    private String month;
    private String exercise;
    private int weight;

    private ArrayList<Integer> log;

    public int getWeight() {
        return weight;
    }

    public String getMonth() {
        return month;
    }

    public String getExercise() {
        return exercise;
    }

    public ProgressMonthlyLog setMonth(String month) {
        this.month = month;
        return this;
    }

    public ProgressMonthlyLog setExercise(String exercise) {
        this.exercise = exercise;
        return this;
    }

    public ProgressMonthlyLog setWeight(int weight) {
        this.weight = weight;
        return this;
    }

    public static ArrayList<ProgressMonthlyLog> getMonthlyLog(Context context,boolean isSingleMonth) {
        ArrayList<ProgressMonthlyLog> progress = new ArrayList<>();
        for (int month : MONTH) {
            for (int exercise : EXERCISE) {
                progress.add(new ProgressMonthlyLog()
                        .setExercise(context.getString(exercise))
                        .setMonth(context.getString(month))
                        .setWeight((int) (Math.random()*100)));
            }
            if(isSingleMonth)
                break;
        }
        return progress;
    }


}

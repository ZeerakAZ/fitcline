package com.az.fitcline.model.dataaccess.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.entities.ProgressMonthlyLog;
import com.az.fitcline.model.dataaccess.entities.ProgressMonthlyWeight;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Wajahat on 25/02/2018.
 */

public class ProgressMonthlyWeightAdapter extends RecyclerView.Adapter <ProgressMonthlyWeightAdapter.ViewHolder> {

    private final IEditWeightListener mListener;
    private ArrayList <ProgressMonthlyWeight> mWeights;
    private Context mContext;

    public ProgressMonthlyWeightAdapter(Context mContext,
                                        ArrayList <ProgressMonthlyWeight> weights,IEditWeightListener listener) {
        this.mWeights = weights;
        this.mContext = mContext;
        mListener = listener;
    }

    @Override
    public ProgressMonthlyWeightAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return new ProgressMonthlyWeightAdapter.ViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.row_monthly_weight;
    }

    public int getItemCount() {
        return mWeights == null ? 0 : mWeights.size();
    }


    public void onBindViewHolder(ProgressMonthlyWeightAdapter.ViewHolder holder, int position) {
        holder.bindData(mWeights.get(position));

    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_month)
        TextView mTvMonth;
        @BindView(R.id.tv_weight)
        TextView mTvWeight;
        @BindView(R.id.rv_workout_images)
        RecyclerView mRvImages;
        @BindView(R.id.tv_header)
        TextView mTvHeader;
        @BindView(R.id.rl_edit)
        RelativeLayout mRlEdit;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindData(ProgressMonthlyWeight progressModel) {

            mTvMonth.setText(progressModel.getMonth());
            mTvWeight.setText(progressModel.getWeight());
            setImagesAdapter(progressModel.getMonthImage());
            mRlEdit.setOnClickListener(view->{
                if (mListener != null) {
                    mListener.editWeight(progressModel);
                }
            });
        }

        private void setImagesAdapter(ArrayList <Integer> monthImage) {
            mRvImages.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            mRvImages.setAdapter(new ProgressMonthlyImageAdapter(mContext, monthImage));
            mTvHeader.setVisibility(getAdapterPosition()==0?View.VISIBLE:View.GONE);


        }


    }

    public interface IEditWeightListener{
        void editWeight(ProgressMonthlyWeight weight);
    }


}

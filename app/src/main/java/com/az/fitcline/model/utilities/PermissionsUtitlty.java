package com.az.fitcline.model.utilities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

/**
 * Created by Administrator on 11/24/2016.
 */

public class PermissionsUtitlty {
    public static final int REQUEST_AUDIO = 0xf01;

    public static boolean isCameraPermissionAllowed(Context context) {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        }

        return false;
    }

    public static boolean isCallPermissionAllowed(Context context) {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        }

        return false;
    }

    public static boolean isAudioPermissionAllowed(Context context) {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.RECORD_AUDIO);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }


    public static boolean isContactsPermisionAllowed(Context context) {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CONTACTS);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        }

        return false;
    }

    public static boolean isExternalStoragePermissionAllowed(Context context) {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        }

        return false;
    }

    public static boolean isLocationPermissionAllowed(Context context) {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        }

        return false;
    }

    public static void showPermissionsDialogForFragment(Fragment fragment, int requestCode, String...permissions) {
        fragment.requestPermissions(permissions, requestCode);

    }

    public static void showPermissionsDialogForActivity(Activity activity, int requestCode , String...permissions) {
        ActivityCompat.requestPermissions(activity,permissions, requestCode);

    }
}

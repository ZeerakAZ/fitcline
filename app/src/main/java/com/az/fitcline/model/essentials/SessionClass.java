package com.az.fitcline.model.essentials;

import android.support.annotation.Nullable;

import com.az.fitcline.model.dataaccess.entities.UserModel;

/**
 * Created by Zeera on 7/18/2017 bt ${File}
 */

public class SessionClass {
    private static final String KEY_USER = "login_user";
    private static SessionClass mInstance;

    private UserModel mUserModel;
    private App mAppContext;
    private String mToken;


    public void setmAppContext(App mAppContext) {
        this.mAppContext = mAppContext;
    }

    private SessionClass() {
        //no instance
    }

    public static SessionClass getInstance() {
        if (mInstance == null) {
            mInstance = new SessionClass();
        }
        return mInstance;
    }

    public void setUser(UserModel user){
        SharedPrefManager.getInstance(mAppContext).
                saveObjectInSharedPref(KEY_USER,user);
    }

    @Nullable
    public UserModel getUser(){
        if(mUserModel==null)
            mUserModel = SharedPrefManager.getInstance(mAppContext).
                    getObjectFromSharedPref(KEY_USER,UserModel.class);
        return mUserModel;
    }

    public String getToken() {
        if(mToken==null && getUser()!=null)
            mToken = getUser().getToken();
        return mToken;
    }

    public SessionClass setToken(String mToken) {
        this.mToken = mToken;
        return this;
    }
}

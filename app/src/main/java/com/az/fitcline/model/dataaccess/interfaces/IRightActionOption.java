package com.az.fitcline.model.dataaccess.interfaces;

import android.support.annotation.DrawableRes;

/**
 * Created by Zeera on 12/17/2017 bt ${File}
 */

public interface IRightActionOption {
    @DrawableRes
    int getRightActionImageId();
    void performRightAction();

}

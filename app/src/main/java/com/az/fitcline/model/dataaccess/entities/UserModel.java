package com.az.fitcline.model.dataaccess.entities;

import com.az.fitcline.R;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Zeera on 12/12/2017 bt ${File}
 */

public class UserModel implements Serializable{

    @SerializedName("Token")
    private String token;
    @SerializedName("userId")
    private String userId;
    @SerializedName(value = "FirstName" ,alternate = "firstName")
    private String firstName;
    @SerializedName(value = "LastName",alternate = "lastName")
    private String lastName;
    @SerializedName(value = "Email" ,alternate = "email")
    private String email;
    @SerializedName(value = "Username" ,alternate = "username")
    private String userName;
    @SerializedName("Password")
    private String password;

    @SerializedName("unit")
    private int unit;
    @SerializedName("birthDate")
    private long birthDate;
    @SerializedName("enablePushNotification")
    private boolean isPushEnabled;



    private static final String[] names= new String[]{
        "Patrick Gomez","Pascal Shmeel","Omair Shah","Zeerak Mushtaq","James Bond",
            "Naruto Uzumaki","Samsung Nokia","Iphone bekaar"
    };

    public static final int[] pics = new int[]{
            R.drawable.user_2,R.drawable.user_3,R.drawable.user_4,R.drawable.user_6,
            R.drawable.user_5,R.drawable.user_7,R.drawable.user_8
    };

    private int profilePicId;

    public UserModel() {
    }

    public UserModel(String userName) {
        this.userName = userName;
    }

    public UserModel(String userName, int profilePicId) {
        this.userName = userName;
        this.profilePicId = profilePicId;
    }

    public String getUserName() {
        return userName;
    }

    public int getProfilePicId() {
        return profilePicId;
    }

    public String getToken() {
        return token;
    }



    public static UserModel getRandomUser(){
        int rand = (int) (Math.random()*(names.length-1));
        return new UserModel(names[rand],pics[rand])  ;
    }

    //-- SETTERS
    public UserModel setToken(String token) {
        this.token = token;
        return this;
    }

    public UserModel setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public UserModel setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public UserModel setEmail(String email) {
        this.email = email;
        return this;
    }

    public UserModel setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public UserModel setPassword(String password) {
        this.password = password;
        return this;
    }
}

package com.az.fitcline.model.dataaccess.entities;

import java.util.Locale;

import javax.annotation.Nonnull;

/**
 * Created by Zeera on 1/6/2018 bt ${File}
 */

public class WorkoutMemberModel {
    private UserModel userModel;
    private String postionChanged;

    public UserModel getUserModel() {
        return userModel;
    }

    @Nonnull
    public String getPositionChanged() {
        if(postionChanged==null)
            return "";
        return postionChanged;
    }

    public static WorkoutMemberModel getWorkoutModel(){
        int rand = ((int) (Math.random() * 100));
        WorkoutMemberModel model = new WorkoutMemberModel();
        model.userModel = UserModel.getRandomUser();
        model.postionChanged = String.format(Locale.ENGLISH,"%s %d",
                rand%2==0?"+":"-",rand);
        return model;
    }
}

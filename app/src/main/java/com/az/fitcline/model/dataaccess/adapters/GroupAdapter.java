package com.az.fitcline.model.dataaccess.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.entities.GroupModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Zeera on 12/18/2017 bt ${File}
 */

public class GroupAdapter extends RecyclerView.Adapter<GroupAdapter.ViewHolder> {
    private final IListViewOptions mListener;
    private List<GroupModel> mGroups;
    private Context mContext;

    public GroupAdapter(Context mContext, List<GroupModel> groups, IListViewOptions listener) {
        this.mGroups = groups;
        this.mContext = mContext;
        mListener = listener;
    }


    @Override
    public GroupAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(viewType, parent, false);
        return new GroupAdapter.ViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.row_group;
    }

    @Override
    public void onBindViewHolder(GroupAdapter.ViewHolder holder, int position) {
        holder.bindData(mGroups.get(position));
    }

    @Override
    public int getItemCount() {
        return mGroups == null ? 0 : mGroups.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_group_tag)
        TextView mTvGroupTag;
        @BindView(R.id.iv_setting_option)
        ImageView mIvSettingOption;
        @BindView(R.id.iv_delete_option)
        ImageView mIvDeleteOption;
        @BindView(R.id.iv_lock)
        ImageView mIvLock;

        ViewHolder(View itemView) {

            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindData(GroupModel group) {
            mTvGroupTag.setText(group.getGroupTag());
            mIvSettingOption.setVisibility(group.isAdmin() ? View.VISIBLE : View.GONE);
            mIvLock.setVisibility(group.isPrivate() ? View.VISIBLE : View.GONE);
            mIvDeleteOption.setVisibility(group.isAdmin() ? View.GONE : View.VISIBLE);
        }

        @OnClick({R.id.iv_setting_option, R.id.iv_delete_option})
        public void onOptionSelected(View view) {
            if (mListener != null && getAdapterPosition() != RecyclerView.NO_POSITION) {
                switch (view.getId()) {
                    case R.id.iv_delete_option:
                        mListener.onDeleteClicked(mGroups.get(getAdapterPosition()));
                        break;
                    case R.id.iv_setting_option:
                        mListener.onSettingClicked(mGroups.get(getAdapterPosition()));
                        break;
                }
            }

        }


    }

    public interface IListViewOptions {
        void onSettingClicked(GroupModel groupModel);

        void onDeleteClicked(GroupModel groupModel);
    }
}

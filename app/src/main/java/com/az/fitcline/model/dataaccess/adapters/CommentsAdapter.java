package com.az.fitcline.model.dataaccess.adapters;

import android.content.Context;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.entities.CommentModel;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Optional;

/**
 * Created by Zeera on 12/10/2017 bt ${File}
 */

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.ViewHolder> {


    @Retention(RetentionPolicy.SOURCE)
    @IntDef({TYPE_COMMENT_DETAILED, TYPE_COMMENT_ONLY})
    @interface AdapterMode {
    }


    private final Context mContext;
    private final List<CommentModel> mComments;
    private final int mType;

    public static final int TYPE_COMMENT_ONLY = 0xf01;
    public static final int TYPE_COMMENT_DETAILED = 0xf02;


    public CommentsAdapter(Context context, List<CommentModel> commentToShow, @AdapterMode int type) {
        mContext = context;
        mComments = commentToShow;
        this.mType = type;
    }

    @Override
    public CommentsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CommentsAdapter.ViewHolder holder, int position) {
        holder.bindData(mComments.get(position));
    }

    @Override
    public int getItemCount() {
        return mComments == null ? 0 : mComments.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mType == TYPE_COMMENT_ONLY ? R.layout.row_comment : R.layout.row_comment_full;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_commenter_name)
        TextView mTvCommenterName;
        @BindView(R.id.tv_comment)
        TextView mTvComment;
        @BindView(R.id.tv_post_time)
        @Nullable
        TextView mTvPostTime;
        @BindView(R.id.iv_profile_pic)
        @Nullable
        ImageView mProfilePic;


        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


        void bindData(CommentModel commentModel) {
            mTvComment.setText(commentModel.getComment());
            mTvCommenterName.setText(commentModel.getUser().getUserName());
            if (mProfilePic != null && getItemViewType() == R.layout.row_comment_full) {
                mProfilePic.setImageResource(commentModel.getUser().getProfilePicId());
            }
        }

    }
}

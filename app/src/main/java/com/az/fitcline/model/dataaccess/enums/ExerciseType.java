package com.az.fitcline.model.dataaccess.enums;

import android.content.Context;

import com.az.fitcline.R;

/**
 * Created by Zeera on 3/4/2018 bt ${File}
 */

public enum ExerciseType {
    BENCH(R.string.progress_exercise_bench),
    SQUAT(R.string.progress_exercise_squat),
    DEAD_LIFT(R.string.progress_exercise_deadlift);
    private int name;

    ExerciseType(int name) {
        this.name = name;
    }

    public String getName(Context context) {
        return context.getString(name);
    }
}

package com.az.fitcline.model.dataaccess.enums;

import android.content.Context;

import com.az.fitcline.R;

/**
 * Created by Zeera on 12/17/2017 bt ${File}
 */

public enum Gender {
    MALE(R.string.ph_male,0),
    FEMALE(R.string.ph_female,1),
    ANY(R.string.ph_any,2);

    Gender(int displayValue, int id) {
        this.displayValue = displayValue;
        this.id = id;
    }

    private int displayValue;
    private int id;

    public String getDisplayValue(Context context) {
        return context.getString(displayValue);
    }
}

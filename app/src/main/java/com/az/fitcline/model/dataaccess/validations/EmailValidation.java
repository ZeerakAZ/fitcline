package com.az.fitcline.model.dataaccess.validations;

import android.widget.TextView;

/**
 * Created by Administrator on 5/12/2017.
 */

public class EmailValidation implements Validation {
    private String errorMessage;
    private TextView textView;
    private String heading;

    public EmailValidation(String errorMessage,  TextView textView,String heading) {
        this.errorMessage = errorMessage;
        this.textView = textView;
        this.heading=heading;
    }

    @Override
    public boolean passes() {
        if(isValidEmail(textView.getText().toString())){
            textView.setError(null);
            return true;
        }else{
            textView.requestFocus();
            textView.setError(errorMessage);
            return false;
        }
    }

    private boolean isValidEmail(CharSequence emailAddress) {
        return emailAddress != null && android.util.Patterns.EMAIL_ADDRESS.matcher(emailAddress).matches();
    }

    @Override
    public String getMessage() {
        return errorMessage;
    }

    @Override
    public String getHeading() {
        return heading;
    }
}

package com.az.fitcline.model.dataaccess.network;

/**
 * Created by Zeera on 6/28/2017 bt ${File}
 */

public interface APIsEndPoints {
    String CREATE_SESSION = "Auth/Createsession";
    String SIGNUP = "Auth/SignUp";
    String LOGIN = "Auth/Login";
    String REQUEST_PASSWORD_RECOVERY = "Auth/RequestPasswordRecovery";
    String VERIFY_PASSWORD_RECOVERY = "Auth/VerifyPasswordRecoveryCode";
    String CHANGE_PASSWORD = "Auth/ChangePassword";
    String COMPLETE_PROFILE = "Auth/CompleteProfile";
    String UPLOAD_FILE = "File/UploadFile";
}
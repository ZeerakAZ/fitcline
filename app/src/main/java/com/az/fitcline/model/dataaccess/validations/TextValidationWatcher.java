package com.az.fitcline.model.dataaccess.validations;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;

/**
 * Created by Administrator on 4/19/2017.
 */

public abstract class TextValidationWatcher implements TextWatcher {

    private final TextView mTextView;

    public TextValidationWatcher(TextView textView) {
        this.mTextView = textView;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    public abstract void validate(TextView textView, String text);

    @Override
    final public void afterTextChanged(Editable s) {
        String text = mTextView.getText().toString();
        validate(mTextView, text);
    }

}

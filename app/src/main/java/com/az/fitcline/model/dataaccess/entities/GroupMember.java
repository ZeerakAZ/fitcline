package com.az.fitcline.model.dataaccess.entities;

import java.io.Serializable;

/**
 * Created by Zeera on 12/23/2017 bt ${File}
 */

public class GroupMember extends UserModel implements Serializable {
    private boolean isAdmin;

    public GroupMember(String userName, int profilePicId) {
        super(userName, profilePicId);
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public static GroupMember getRandomGroupMember() {
        UserModel userModel = getRandomUser();
        GroupMember member = new GroupMember(userModel.getUserName(), userModel.getProfilePicId());
        member.setIsAdmin( ((int) (Math.random() * 10) )%2 == 0);
        return member;
    }
}

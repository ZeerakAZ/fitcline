package com.az.fitcline.model.dataaccess.entities;

/**
 * Created by Zeera on 1/27/2018 bt ${File}
 */

public class NotificationModel {
    private static String[] notificationText = new String[]{
            "liked your image", "commented on your status", "Hurray!!Got a offer for you", "Killed you",
            "Has birthday today", "mentioned you in a post", "tagged you",
            };
    private String notificationMessage;
    private UserModel user;


    public NotificationModel(String notificationMessage, UserModel user) {
        this.notificationMessage = notificationMessage;
        this.user = user;
    }


    public static  NotificationModel getRandomNotification(){
        int rand = ((int) (Math.random() * (notificationText.length-1)));
        return new NotificationModel(notificationText[rand],UserModel.getRandomUser());
    }

    public String getNotificationMessage() {
        return notificationMessage;
    }

    public UserModel getUser() {
        return user;
    }
}

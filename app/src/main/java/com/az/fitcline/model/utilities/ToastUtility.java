package com.az.fitcline.model.utilities;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Administrator on 2/23/2017.
 */

public class ToastUtility {

    private static final boolean SHOULD_SHOW_TOAST = true;

    public static void showToastForShortTime(Context context, String message){
        if (SHOULD_SHOW_TOAST) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }
    public static void showToastForLongTime(Context context, String message){
        if (SHOULD_SHOW_TOAST) {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
    }
    public static void showToastForShortTime(Context context, String message, boolean shouldShowToast){
        if (shouldShowToast) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }
    public static void showToastForLongTime(Context context, String message, boolean shouldShowToast){
        if (shouldShowToast) {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
    }
}

package com.az.fitcline.model.dataaccess.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zeera on 12/13/2017 bt ${File}
 */

public class ImageModel {
    private String imagePath;
    private String thumbPath;

    public ImageModel(String imagePath, String thumbPath) {
        this.imagePath = imagePath;
        this.thumbPath = thumbPath;
    }

    public static List<ImageModel> getImagesData(){
        List<ImageModel> mImages= new ArrayList<>();
        int count = (int) (Math.random() * 10);
        for (int i = 0; i <count ; i++) {
            mImages.add(new ImageModel("",""));
        }
        return mImages;
    }
}

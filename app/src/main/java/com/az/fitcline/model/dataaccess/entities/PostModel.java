package com.az.fitcline.model.dataaccess.entities;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Zeera on 12/10/2017 bt ${File}
 */

public class PostModel implements Serializable{

    private static final String[] posts= new String[]{
            "Full body short week plan","Day 1: Chest and shoulders\u0003Day 2: Back and arms\n" +
            "Day 3: Legs and abs\n" +
            "\n" +
            "I typically do this Monday, Wednesday and Friday. \n" +
            "\n" +
            "Day 1\n" +
            "Bench press 3x10\u0003Seated flys 3x10\u0003Seated military presses 3x10\n" +
            "\n" +
            "Day 2\n" +
            "Weighted pull downs 3x10\n" +
            "Seated curls 3x5\u0003Skull crushers 3x20\u0003\n" +
            "Day 3\u0003Hanging leg raises till failure\u0003Squats 10 sets till failure",

            "Highway to 1st place","Just a heads up, gym will close this week at 7pm for pool construction.",
            "Alright one more\u00033x8 squats\n" +
                    "3x5 lateral raise heavy weight\n" +
                    "5x10 cable curls\n" +
                    "5x10 cable french curls\n" +
                    "...",
            "Took this pic of","At Google I/O 2017, the Android Framework team announced the new Android Architecture Components.",
            "Machine Gun Kelly, X Ambassadors & Bebe Rexha - Home (from Bright: The Album) [Music Video]"
    };
    private String post;
    private UserModel userModel;
    private String videoPath;
    private String imagePath;
    private int likesCount;
    private int commentsCount;
    private String postTime;
    private ArrayList<CommentModel> comments;

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Integer getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(Integer likesCount) {
        this.likesCount = likesCount;
    }

    public Integer getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(Integer commentsCount) {
        this.commentsCount = commentsCount;
    }

    public ArrayList<CommentModel> getComments() {
        return comments;
    }

    public void setComments(ArrayList<CommentModel> comments) {
        this.comments = comments;
    }

    public String getPostTime() {
        return postTime;
    }

    public void setPostTime(String postTime) {
        this.postTime = postTime;
    }


    public PostModel(){

    }

    PostModel(PostModel model){
        this.post = model.post;
        this.likesCount = model.likesCount;
        this.commentsCount = model.commentsCount;
        this.userModel = model.userModel;
    }

    public static PostModel getRandomPosts(){
        int randomInt = (int) (Math.random()*(posts.length-1));
        PostModel model = new PostModel();
        model.post = posts[randomInt];
        model.likesCount = randomInt%2==0?randomInt*100:randomInt*1000;
        model.commentsCount = randomInt%2==0?randomInt*10:randomInt*8;
        model.videoPath = randomInt%2==0?null:"some video path";
        model.imagePath = randomInt%4==0?"some path":null;
        model.userModel = UserModel.getRandomUser();
        model.postTime = randomInt +" minutes ago";
        model.comments = new ArrayList<>();
        if (randomInt%2==0) {
            for (int i = 0; i < randomInt; i++) {
                model.comments.add(CommentModel.getRandomComment());
            }
        }
        return model;
    }
}

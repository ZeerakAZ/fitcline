package com.az.fitcline.model.dataaccess.interfaces;

import com.az.fitcline.views.fragments.BaseFragment;

/**
 * Created by Zeera on 11/19/2017 bt ${File}
 */

public interface IFragmentUpdateListener {
    void fragmentUpdated(BaseFragment fragment);
}

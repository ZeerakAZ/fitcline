package com.az.fitcline.model.dataaccess.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.entities.WorkoutType;
import com.az.fitcline.model.dataaccess.enums.ProgressType;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Wajahat on 23/02/2018.
 */

public class WorkoutTypeAdapter extends RecyclerView.Adapter<WorkoutTypeAdapter.ViewHolder> {


    private final ITypeListener mListener;
    private ArrayList<WorkoutType> mWorkout;
    private Context mContext;
    @ProgressType
    private int mType;

    public WorkoutTypeAdapter(Context mContext,ITypeListener listener) {
        this.mWorkout = WorkoutType.getTypeForProgress(mContext);
        this.mContext = mContext;
        mListener = listener;

        mType = ProgressType.TYPE_STRENGTH;
        if (mListener != null) {
            mListener.typeUpdated(ProgressType.TYPE_STRENGTH);
        }
    }

    @Override
    public WorkoutTypeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return new WorkoutTypeAdapter.ViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.row_progress_strength;
    }

    @Override
    public int getItemCount() {
        return mWorkout == null ? 0 : mWorkout.size();
    }

    @Override
    public void onBindViewHolder(WorkoutTypeAdapter.ViewHolder holder, int position) {
        holder.bindData(mWorkout.get(position));

    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_workout_type)
        TextView mTvWorkoutType;
        @BindView(R.id.iv_arrow)
        ImageView mIvArrow;
        @BindView(R.id.divider)
        View mDivider;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindData(WorkoutType workoutType) {
            mTvWorkoutType.setText(workoutType.getTypeName());
            mTvWorkoutType.setOnClickListener(view -> {
                mType = mWorkout.get(getAdapterPosition()).getId();
                notifyDataSetChanged();
                if (mListener != null) {
                    mListener.typeUpdated(mType);
                }
            });

            if (workoutType.getId() == mType) {
                mTvWorkoutType.setTextColor(ContextCompat.getColor(mContext, R.color.apple_green));
                mIvArrow.setVisibility(View.VISIBLE);
            } else {
                mTvWorkoutType.setTextColor(ContextCompat.getColor(mContext, R.color.dimgray));
                mIvArrow.setVisibility(View.INVISIBLE);
            }

            mDivider.setVisibility(getAdapterPosition() == getItemCount() - 1 ? View.GONE : View.VISIBLE);
        }
    }

    public interface ITypeListener {
        void typeUpdated(@ProgressType int type);
    }


}

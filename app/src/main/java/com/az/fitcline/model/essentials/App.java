package com.az.fitcline.model.essentials;

import android.app.Application;
import android.content.Context;

import com.facebook.stetho.Stetho;

/**
 * Created by Zeera on 10/29/2017 bt ${File}
 */

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
        SessionClass.getInstance().setmAppContext(this);
    }



    @Override
    public Context getApplicationContext() {
        return super.getApplicationContext();
    }
}

package com.az.fitcline.model.dataaccess.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.entities.GroupMember;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Zeera on 12/23/2017 bt ${File}
 */

public class GroupMemberAdapter extends RecyclerView.Adapter<GroupMemberAdapter.ViewHolder> {
    private final GroupMemberAdapter.IListViewOptions mListener;
    private List<GroupMember> mGroups;
    private Context mContext;

    public GroupMemberAdapter(Context mContext, List<GroupMember> groups,
                              GroupMemberAdapter.IListViewOptions listener) {
        this.mGroups = groups;
        this.mContext = mContext;
        mListener = listener;
    }


    @Override
    public GroupMemberAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(viewType, parent, false);
        return new GroupMemberAdapter.ViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.row_group_member;
    }

    @Override
    public void onBindViewHolder(GroupMemberAdapter.ViewHolder holder, int position) {
        holder.bindData(mGroups.get(position));
    }

    @Override
    public int getItemCount() {
        return mGroups == null ? 0 : mGroups.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_username)
        TextView mTvUsername;
        @BindView(R.id.tv_admin)
        TextView mTvAdmin;
        @BindView(R.id.iv_delete_option)
        ImageView mIvDeleteOption;

        ViewHolder(View itemView) {

            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindData(GroupMember groupMember) {
            mTvUsername.setText(groupMember.getUserName());
            mTvAdmin.setVisibility(groupMember.isAdmin() ? View.VISIBLE : View.GONE);
            mIvDeleteOption.setVisibility(groupMember.isAdmin() ? View.GONE : View.VISIBLE);
        }

        @OnClick({R.id.iv_setting_option, R.id.iv_delete_option})
        public void onOptionSelected(View view) {
            if (mListener != null && getAdapterPosition() != RecyclerView.NO_POSITION) {
                switch (view.getId()) {
                    case R.id.iv_delete_option:
                        mListener.onDeleteClicked(mGroups.get(getAdapterPosition()));
                        break;
                    case R.id.iv_setting_option:
                        mListener.onSettingClicked(mGroups.get(getAdapterPosition()));
                        break;
                }
            }

        }


    }

    public interface IListViewOptions {
        void onSettingClicked(GroupMember groupMember);

        void onDeleteClicked(GroupMember groupMember);
    }
}

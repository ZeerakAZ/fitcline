package com.az.fitcline.model.essentials.service

import android.app.IntentService
import android.content.Intent
import android.content.Context
import android.content.ServiceConnection
import android.os.Binder
import android.os.IBinder
import android.support.v4.content.LocalBroadcastManager
import com.az.fitcline.model.dataaccess.network.*
import com.az.fitcline.model.essentials.SessionClass
import com.az.fitcline.model.utilities.LogUtility
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONArray
import org.json.JSONObject
import java.io.File

private const val PARAM_FILE_PATH = "param_file_path"
private const val PARAM_IDENTIFIER = "param_identifier";

const val ACTION_FILE_UPLOAD = "action_file_upload"
const val KEY_PROGRESS = "key_progress"
const val KEY_SERVER_PATH = "key_server_path"
const val KEY_IDENTIFIER = "key_identifier"


class FileUploadService : IntentService("FileUploadService"), ProgressRequestBody.UploadCallbacks, IResponse {

    private var mApiCaller: CallApi? = null
    private var progressMap: HashMap<String, Int> = HashMap()
    private val mBinder: IBinder = LocalBinder()

    override fun onProgressUpdate(percentage: Int, filePath: String) {

        progressMap[filePath] = percentage

        if (percentage % 4 == 0)
            broadCastUpdates(percentage, null, filePath)
        LogUtility.debugLog("$percentage uploaded of $filePath")
    }

    override fun onHandleIntent(intent: Intent?) {
        val path = intent?.extras?.getString(PARAM_FILE_PATH)
        uploadFile(path)
    }

    override fun onCreate() {
        super.onCreate()
        LogUtility.debugLog("Upload file Service Created")
    }

    override fun onDestroy() {
        super.onDestroy()
        LogUtility.debugLog("Upload file Service Destroyed")
    }

    private fun uploadFile(path: String?) {
        LogUtility.debugLog("$path started uploading")

        val file = File(path)
        val requestFile = ProgressRequestBody(file, this)
        val body = MultipartBody.Part.createFormData("fileName", file.name, requestFile)
        val token = RequestBody.create(MediaType.parse("text/plain"), SessionClass.getInstance().token)

        val uploadFile = ApiClient.getFileUploadClient()
                .create(IApiInterface::class.java).uploadFile(token, body)


        mApiCaller = CallApi(this, this)
        mApiCaller?.setmOverrideSuccessCallback { response, _ ->
            LogUtility.debugLog("$path  uploaded successfully")
            val jsonObj = JSONObject(response)
            val serverPath = jsonObj.getString("id")
            broadCastUpdates(100, serverPath, path)
        }?.callService(uploadFile, APIsEndPoints.UPLOAD_FILE)

    }

    override fun onSuccess(body: String?, endPoint: String?) {

    }

    override fun onError(error: String?, endPoint: String?) {

    }

    override fun onFailed(e: Throwable?, endPoint: String?) {

    }

    public fun getProgress(filePath: String): Int? {
        return progressMap[filePath]
    }

    override fun onBind(intent: Intent?): IBinder {
        return mBinder
    }

    private fun broadCastUpdates(progress: Int,
                                 serverPath: String?,
                                 identifier: String?) {
        val intent = Intent().apply {
            action = ACTION_FILE_UPLOAD
            putExtra(KEY_IDENTIFIER, identifier)
            putExtra(KEY_SERVER_PATH, serverPath)
            putExtra(KEY_PROGRESS, progress)
        }
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    inner class LocalBinder : Binder() {
        val service: FileUploadService
            get() = this@FileUploadService
    }


    companion object {
        @JvmStatic
        fun startUploading(context: Context, filePath: String,
                           serviceConnection: ServiceConnection?) {
            val intent = Intent(context, FileUploadService::class.java).apply {
                putExtra(PARAM_FILE_PATH, filePath)
            }
            context.startService(intent)
            //context.bindService(intent,serviceConnection,Context.BIND_AUTO_CREATE)
        }

    }

    public interface IUploadListener {
        fun onUploadUpdated(filePath: String, progress: Int, serverPath: String?)
    }
}

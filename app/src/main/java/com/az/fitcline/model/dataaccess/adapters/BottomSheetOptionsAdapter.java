package com.az.fitcline.model.dataaccess.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.entities.BottomSheetOptionModel;
import com.az.fitcline.model.dataaccess.entities.ImageModel;
import com.az.fitcline.views.dialogs.GenericBottomSheetDialog;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zeera on 12/16/2017 bt ${File}
 */

public class BottomSheetOptionsAdapter extends RecyclerView.Adapter<BottomSheetOptionsAdapter.ViewHolder> {
    private final GenericBottomSheetDialog.IBottomOptionSelectionListener mListener;
    private List<BottomSheetOptionModel> mOptions;
    private Context mContext;

    public BottomSheetOptionsAdapter(Context mContext, List<BottomSheetOptionModel> mImages,
                                     GenericBottomSheetDialog.IBottomOptionSelectionListener listener) {
        this.mOptions = mImages;
        this.mContext = mContext;
        mListener = listener;
    }


    @Override
    public BottomSheetOptionsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(viewType, parent, false);
        return new BottomSheetOptionsAdapter.ViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.row_bottom_option;
    }

    @Override
    public void onBindViewHolder(BottomSheetOptionsAdapter.ViewHolder holder, int position) {
        holder.bindData(mOptions.get(position));
    }

    @Override
    public int getItemCount() {
        return mOptions == null ? 0 : mOptions.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_option)
        TextView mTvOption;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mTvOption.setOnClickListener(v -> {
                if (getAdapterPosition() != RecyclerView.NO_POSITION && mListener != null) {
                    mListener.performActionPlease(mOptions.get(getAdapterPosition())
                            .getUniqueId());
                }
            });
        }

        void bindData(BottomSheetOptionModel option) {
            mTvOption.setText(option.getOptionNameId());

        }
    }
}
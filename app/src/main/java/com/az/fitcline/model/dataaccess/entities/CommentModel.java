package com.az.fitcline.model.dataaccess.entities;

import java.io.Serializable;

/**
 * Created by Zeera on 12/10/2017 bt ${File}
 */

public class CommentModel implements Serializable{
    private static final String[] comments= new String[]{
            "I’m up for that. Didn’t we try this out sometime last year tho",
            "@matty_mcfly93 come on we know you’re going to bail like the last 20 times",
            "@tennis76 don’t get me started about yesterday",
            "Lmk when you’re doing shoulders, I’m down. Have done it a few times before, phenomenal workout.",
            "@matty_mcfly93 LOL",
            "Doesn’t seem that chellenging but looks like a good plan",
            "Well  played rogers","well, it can improves more"
    };
    private String comment;
    private UserModel user;
    private String commentTime;


    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public UserModel getUser() {
        return user;
    }

    public void setCommenterName(UserModel commenterName) {
        this.user = commenterName;
    }

    public CommentModel(String comment, UserModel user) {
        this.comment = comment;
        this.user = user;
    }

    public static CommentModel getRandomComment(){
        return new CommentModel(comments[(int) (Math.random()*(comments.length-1))],
                UserModel.getRandomUser());
    }
}

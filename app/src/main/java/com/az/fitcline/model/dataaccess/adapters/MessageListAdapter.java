package com.az.fitcline.model.dataaccess.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.entities.MessageModel;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zeera on 1/16/2018 bt ${File}
 */

public class MessageListAdapter extends RecyclerView.Adapter<MessageListAdapter.ViewHolder> {
    private List<MessageModel> mMessages;
    private Context mContext;

    public MessageListAdapter(Context mContext, List<MessageModel> mMessages) {
        this.mMessages = mMessages;
        this.mContext = mContext;
    }

    @Override
    public MessageListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(viewType, parent, false);
        return new MessageListAdapter.ViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.row_message_list_me;
    }

    @Override
    public void onBindViewHolder(MessageListAdapter.ViewHolder holder, int position) {
        holder.bindData(mMessages.get(position));
    }

    @Override
    public int getItemCount() {
        return mMessages == null ? 0 : mMessages.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_message)
        TextView mTvMessage;
        @BindView(R.id.tv_message_time)
        TextView mTvMessageTime;
        @BindView(R.id.tv_username)
        TextView mTvUserName;
        @BindView(R.id.iv_profile_pic)
        ImageView mIvProfilePic;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindData(MessageModel messageModel) {
            mTvUserName.setText(messageModel.getUser().getUserName());
            mIvProfilePic.setImageResource(messageModel.getUser().getProfilePicId());
            mTvMessage.setText(messageModel.getMessageText());
            mTvMessageTime.setText(String.format(Locale.ENGLISH, "%d mins",
                    messageModel.getMessageTime()));
        }
    }
}

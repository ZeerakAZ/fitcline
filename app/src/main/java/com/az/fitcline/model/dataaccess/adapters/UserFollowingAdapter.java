package com.az.fitcline.model.dataaccess.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.entities.UserModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UserFollowingAdapter extends RecyclerView.Adapter<UserFollowingAdapter.ViewHolder> {
    private final List<UserModel> mUsers;
    private final List<Integer> mFollowingIds;
    private final Context mContext;

    public UserFollowingAdapter(Context context, List<UserModel> users, List<Integer> followingIds) {
        mContext = context;
        mUsers = users;
        this.mFollowingIds = followingIds;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(viewType, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public int getItemViewType(int position) {
        return R.layout.row_user_follow;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.bindData(mUsers.get(position));
    }

    @Override
    public int getItemCount() {
        return mUsers == null ? 0 : mUsers.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_profile_pic)
        ImageView mIvProfileImage;
        @BindView(R.id.tv_username)
        TextView mTvUserName;
        @BindView(R.id.tv_follow)
        TextView mTvFollow;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, itemView);
        }

        void bindData(UserModel userModel) {
            mIvProfileImage.setImageResource(userModel.getProfilePicId());
            mTvUserName.setText(userModel.getUserName());
            if (mFollowingIds.contains(userModel.getProfilePicId())) {
                mTvFollow.setText(R.string.status_following);
                mTvFollow.setTextColor(ContextCompat.getColor(mContext, R.color.mediumjunglegreen));
            } else {
                mTvFollow.setText(R.string.status_follow);
                mTvFollow.setTextColor(ContextCompat.
                        getColor(mContext, R.color.android_green));
            }
        }

        @OnClick(R.id.tv_follow)
        public void updateFollowing() {

            int profilePicId = mUsers.get(getAdapterPosition()).getProfilePicId();
            if (mFollowingIds.contains(profilePicId))
                mFollowingIds.remove(mFollowingIds.indexOf(profilePicId));
            else
                mFollowingIds.add(profilePicId);
            notifyItemChanged(getAdapterPosition());
        }

    }
}

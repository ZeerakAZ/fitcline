package com.az.fitcline.model.utilities;

import android.util.Log;


/**
 * Created by ZeeraMu on 2/23/2017 android
 */

public final class LogUtility {

    private static final boolean SHOULD_SHOW_LOGS = true;
    private static final String TAG = "fitcline-all-LOG";

    public static  void errorLog(String message) {
        if (SHOULD_SHOW_LOGS)
            Log.e(TAG, message==null?"error message is null":message);
    }



    public static  void debugLog(String message) {
        if (SHOULD_SHOW_LOGS)
            Log.d(TAG, getInfo()+message);
    }

    public static  void infoLog(String message) {
        Log.d(TAG, message);
    }

    public static  void debugLog(String message, String... value) {

        String completeValue = "";

        for (String s : value) {
            completeValue += s + " \n";
        }
        if (SHOULD_SHOW_LOGS)
            Log.d(TAG, getInfo()+message + "--->" + completeValue);

    }

    private static String getInfo(){
        final StackTraceElement stackTrace = new Exception().getStackTrace()[2];
        String fileName = stackTrace.getFileName();
        if (fileName == null) fileName="";
        return stackTrace.getMethodName() + " (" + fileName + ":"
                + stackTrace.getLineNumber() + ") ";
    }


    public static  void horribleSituationLog(String message) {
        if (SHOULD_SHOW_LOGS)
            Log.wtf(TAG, message);
    }


}

package com.az.fitcline.model.dataaccess.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.az.fitcline.R;
import com.az.fitcline.model.dataaccess.entities.MessageModel;
import com.az.fitcline.model.dataaccess.entities.NotificationModel;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zeera on 1/16/2018 bt ${File}
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {
    private List<NotificationModel> mNotification;
    private Context mContext;

    public NotificationAdapter(Context mContext, List<NotificationModel> mMessages) {
        this.mNotification = mMessages;
        this.mContext = mContext;
    }




    @Override
    public NotificationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(viewType, parent, false);
        return new NotificationAdapter.ViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.row_notification;
    }

    @Override
    public void onBindViewHolder(NotificationAdapter.ViewHolder holder, int position) {
        holder.bindData(mNotification.get(position));
    }

    @Override
    public int getItemCount() {
        return mNotification == null ? 0 : mNotification.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_notification)
        TextView mTvNotification;
        @BindView(R.id.tv_time)
        TextView mTvNotifcaitonTime;
        @BindView(R.id.tv_username)
         TextView mTvUserName;
        @BindView(R.id.iv_profile_pic)
        ImageView mIvProfilePic;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindData(NotificationModel notificationModel) {
            mTvNotification.setText(notificationModel.getNotificationMessage());
            mTvNotifcaitonTime.setText(String.format(Locale.ENGLISH, "%d mins",
                    3));
            mTvUserName.setText(notificationModel.getUser().getUserName());
            mIvProfilePic.setImageResource(notificationModel.getUser().getProfilePicId());
        }
    }
}

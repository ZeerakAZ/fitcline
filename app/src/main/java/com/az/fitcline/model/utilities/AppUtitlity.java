package com.az.fitcline.model.utilities;

import java.util.Locale;

/**
 * Created by zeerak on 6/2/2018 bt ${File}
 */
public class AppUtitlity {
    public static String getCultureCode() {
        return Locale.getDefault().getLanguage() + "-" +
                Locale.getDefault().getCountry();
    }
}
